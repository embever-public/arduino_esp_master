#!/bin/sh

PURPLE=$(tput setaf 125)
YELLOW=$(tput setaf 3)
DEFAULT=$(tput sgr 0)

PERMISSION=n
while [ "$PERMISSION" != "y" ]
do
    echo "\nHello,`whoami`. Please, enter the path to your Arduino/libraries folder."
    echo "e.g. "$YELLOW"/Users/`whoami`/Documents/Arduino/libraries/"$DEFAULT""
    read USER_ARDUINO_PATH
    echo "Do you want to make links to all of the inc/, src/ files into"
    echo ""$YELLOW""$USER_ARDUINO_PATH""$DEFAULT" "$YELLOW"[y/n]?"$DEFAULT""
    read PERMISSION
done

PROJECT_PATH="$PWD"
PROJECT_INC_PATH=${PROJECT_PATH}/inc
PROJECT_SRC_PATH=${PROJECT_PATH}/src

# get_files_in_src_dir () {
#   files_src="`ls *.cpp`"
# }
# get_files_in_inc_dir () {
#   files_inc="`ls *.h`"
# }

# cd ${PROJECT_SRC_PATH}
# get_files_in_src_dir
# cd ${PROJECT_INC_PATH}
# get_files_in_inc_dir

cd "$USER_ARDUINO_PATH"
mkdir arduino_esp_master
USER_ARDUINO_ESP_MASTER_PATH=${USER_ARDUINO_PATH}/arduino_esp_master
cd "$USER_ARDUINO_ESP_MASTER_PATH"

ln -s ${PROJECT_INC_PATH}/CException.h
ln -s ${PROJECT_INC_PATH}/arduino_esp_master.h
ln -s ${PROJECT_INC_PATH}/ebvboard.h
ln -s ${PROJECT_INC_PATH}/ebvconf.h
ln -s ${PROJECT_INC_PATH}/ebvcrc.h
ln -s ${PROJECT_INC_PATH}/ebverr.h
ln -s ${PROJECT_INC_PATH}/ebvesp.h
ln -s ${PROJECT_INC_PATH}/ebvi2c.h
ln -s ${PROJECT_INC_PATH}/ebvi2chal.h
ln -s ${PROJECT_INC_PATH}/ebvmpack.h
ln -s ${PROJECT_INC_PATH}/extcwpack.h
ln -s ${PROJECT_INC_PATH}/extcwpack_defines.h

ln -s ${PROJECT_SRC_PATH}/CException.cpp
ln -s ${PROJECT_SRC_PATH}/arduino_esp_master.cpp
ln -s ${PROJECT_SRC_PATH}/ebvboard.cpp
ln -s ${PROJECT_SRC_PATH}/ebvcrc.cpp
ln -s ${PROJECT_SRC_PATH}/ebvesp.cpp
ln -s ${PROJECT_SRC_PATH}/ebvi2c.cpp
ln -s ${PROJECT_SRC_PATH}/ebvi2chal.cpp
ln -s ${PROJECT_SRC_PATH}/ebvmpack.cpp
ln -s ${PROJECT_SRC_PATH}/extcwpack.cpp



