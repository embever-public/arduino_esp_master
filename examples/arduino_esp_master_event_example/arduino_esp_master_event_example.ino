#include "arduino_esp_master.h"

#ifndef ARDUINO_ESP_MASTER_SERIAL_DEBUG
#define ARDUINO_ESP_MASTER_SERIAL_DEBUG
#endif



/* Arduino related structures*/
enum {
    ARDUINO_UART_BAUD_RATE = 115200,              
    ARDUINO_LOOP_CYCLE_DELAY = 60000,               // Delay before new main loop cycle shall start, default: 60000    
};




void setup() {
/* To Enable Debug messages over the UART Serial
 * Monitor see the header "arduino_esp_master.h"
 * and define ARDUINO_ESP_MASTER_SERIAL_DEBUG
 */
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
  Serial.begin(ARDUINO_UART_BAUD_RATE);
  while(!Serial) {
    ;
  }
#endif
  arduino_esp_pins_init();
  arduino_esp_master_init();
}

void loop() {
  get_esp_slave_ready();
  /* 
   *  Insert the application specific code here
   */
  if (!arduino_esp_master_check_wait_slave_ready()) {
    get_esp_slave_ready();
    return;
  }
  /* ATTENTION:
   * For the moment the size of final data, sent to the device, is
   * restricted to ARDUINO_ESP_MASTER_I2C_MAX_MPACK_SIZE (28B)
   * This is why the names of the Event Parameter and Event itself
   * should be as short as possible (but at least 3 characters long). 
   * The value of the Event Parameter should contain positive unsigned integer
   * 
   * The example below is suitable for the application:
   */
  
  char event_name[] = "test";
  char param_name[] = "bugs";
  uint64_t number_of_bugs = 409;
  bool event_built;
  bool event_sent;
  event_built = arduino_esp_master_build_event(event_name, sizeof(event_name), 
                                  param_name, sizeof(param_name), number_of_bugs);
  if (event_built) {
    event_sent = arduino_esp_master_send_event();
    if (event_sent) {
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
      Serial.print("Event is SET\n");
#endif
      delay(ARDUINO_LOOP_CYCLE_DELAY);
      return;
    } else {
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
      Serial.print("Event is NOT SET\n");
#endif
      delay(ARDUINO_LOOP_CYCLE_DELAY);
      return;
    }
  } else {
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
    Serial.print("Event build is FAILED\n");
#endif
    delay(ARDUINO_LOOP_CYCLE_DELAY);
    return;
  }
}
