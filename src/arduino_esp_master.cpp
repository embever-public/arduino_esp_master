/** arduino_esp_master.cpp
 *  
 *  Contains functions to put an Event
 *  
 *  Created on: 31.01.2020
 *  Author: Yuriy Golotsevich
 */

#include "arduino_esp_master.h"




/* DECLARATION OF STATIC FUNCTIONS */

/* Arduino related */



/* ESP related */
static bool arduino_esp_master_wait_slave_ready_for_cmd(uint32_t timeout);
static bool read_delayed_response_procedure(void);
static void wake_esp_slave(void);
static void reset_esp_slave(void);




/* DEFINITION OF VARIABLES, BUFFERS AND STRUCTURES */

/* Arduino related */

/* ESP related */
static char _errmsg[130];



/* ESP related */
static uint8_t _dresp_data[ARDUINO_ESP_MASTER_I2C_MAX_DATA_SIZE];
uint8_t * dresp_data = _dresp_data;
static uint16_t dresp_length;
static struct ebv_mpack _arduino_esp_master_mpack;
struct ebv_mpack *mpack = &_arduino_esp_master_mpack;
static uint8_t _arduino_esp_master_mpack_tx_buf[ARDUINO_ESP_MASTER_I2C_MAX_MPACK_SIZE];
static uint8_t _arduino_esp_master_mpack_rx_buf[ARDUINO_ESP_MASTER_I2C_MAX_MPACK_SIZE];
uint8_t *arduino_esp_master_mpack_tx_buf = (uint8_t *)_arduino_esp_master_mpack_tx_buf;
uint8_t *arduino_esp_master_mpack_rx_buf = (uint8_t *)_arduino_esp_master_mpack_rx_buf;




/* DEFINITION OF EXTERNAL FUNCTIONS */

/* Arduino related */

void arduino_esp_pins_init(void) {
  /* All the pins below are "active LOW" */
  pinMode(ARDUINO_ESP_MASTER_READY_PIN, INPUT);
  pinMode(ARDUINO_ESP_MASTER_IRQ_PIN, INPUT);
  
  pinMode(ARDUINO_ESP_MASTER_WAKE_SLAVE_PIN, OUTPUT);
  digitalWrite(ARDUINO_ESP_MASTER_WAKE_SLAVE_PIN, HIGH);
  
  pinMode(ARDUINO_ESP_MASTER_RESET_SLAVE_PIN, OUTPUT);
  digitalWrite(ARDUINO_ESP_MASTER_RESET_SLAVE_PIN, HIGH);
}

void check_pins(void) {
  if (digitalRead(ARDUINO_ESP_MASTER_IRQ_PIN)) {
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
    Serial.print("\nIRQ pin: HIGH\n");
#endif
  } else {
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
    Serial.print("\nIRQ pin: LOW\n");
#endif
  }
  if (digitalRead(ARDUINO_ESP_MASTER_READY_PIN)) {
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
    Serial.print("READY pin: HIGH\n\n");
#endif
  } else {
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
    Serial.print("READY pin: LOW\n\n");
#endif
  }
}

bool get_esp_slave_ready(void) {
  if (!arduino_esp_master_check_wait_slave_ready()) {
    wake_esp_slave();     // Try to wake ESP Slave if it doesn't respond
    delay(ARDUINO_ESP_MASTER_SLAVE_READY_TIMEOUT);
    if (!arduino_esp_master_check_wait_slave_ready()) {
      reset_esp_slave();  // Try to reset ESP Slave if it doesn't respond
      delay(2*ARDUINO_ESP_MASTER_SLAVE_READY_TIMEOUT);
      if (!arduino_esp_master_check_wait_slave_ready()) {
        return false;
      } 
    } 
  }
  return true;
}

void printHex(uint8_t one_byte) {
  char hex[2];
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
  sprintf(hex, "%02X", one_byte);
  Serial.print(hex);
#endif
}

void printHex_buf(uint8_t * buf, uint8_t buf_len) {
  for (int i=0; i<=buf_len; i++) {
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
    printHex(buf[i]);
    Serial.print(" ");
#endif
  }
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
  Serial.print("\n");
#endif
}

/* ESP related */

void arduino_esp_master_init(void) {
  CEXCEPTION_T err;
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
  Serial.print("MASTER INIT\n");
#endif

#if 1 // Temporary solution due to the issue with error handlers
      // See next "if" condition
  ebv_esp_init_master(ARDUINO_ESP_MASTER_IRQ_PIN, ARDUINO_ESP_MASTER_READY_PIN);
  ebv_mpack_init(mpack, arduino_esp_master_mpack_rx_buf, sizeof(_arduino_esp_master_mpack_rx_buf), 
                        arduino_esp_master_mpack_tx_buf, sizeof(_arduino_esp_master_mpack_tx_buf));
#endif
#if 0 //Always catch an error: -2667
  Try {
    ebv_esp_init_master(ARDUINO_ESP_MASTER_IRQ_PIN, ARDUINO_ESP_MASTER_READY_PIN);
    ebv_mpack_init(mpack, arduino_esp_master_mpack_rx_buf, sizeof(_arduino_esp_master_mpack_rx_buf), 
                          arduino_esp_master_mpack_tx_buf, sizeof(_arduino_esp_master_mpack_tx_buf));
  } Catch(err) {
    sprintf(_errmsg, "Unexpected ERROR in ESP Master Initialisation: %d\n", err);
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
    Serial.print(_errmsg);
#endif
  }
#endif
}

bool arduino_esp_master_check_wait_slave_ready(void) {
    if (ebv_esp_is_interface_ready()) {
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
      Serial.print("EBV IS READY\n");
#endif
    } else {
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
      Serial.print("EBV IS NOT READY\n");
#endif
      check_pins();
      delay(ARDUINO_ESP_MASTER_SLAVE_READY_TIMEOUT);
      return false;
    }
    return true;
}

bool arduino_esp_master_build_event(char * event_name, uint8_t event_name_size, 
                                    char * parameter_name, uint8_t parameter_name_size,
                                    uint64_t parameter_value) {                
    if ((event_name_size + parameter_name_size + sizeof(parameter_value)) > ARDUINO_ESP_MASTER_I2C_MAX_DATA_SIZE) {
        return false;
    } else {
        mpack->pack->current = mpack->pack->start;
        cw_pack_array_size(mpack->pack, 1);
        cw_pack_array_size(mpack->pack, 2);
        ebv_mpack_add_str(mpack, event_name);
        cw_pack_map_size(mpack->pack, 1);
        ebv_mpack_add_str(mpack, parameter_name);
        cw_pack_unsigned(mpack->pack, parameter_value);
    }
    return true;
}

bool arduino_esp_master_send_event(void) {
#if 1 // Temporary solution due to the issue with error handlers
      // See next "if" condition
  ebv_esp_cmd_put_events((uint8_t *)mpack->pack->start, mpack->pack->current - mpack->pack->start, 
                                                                  ARDUINO_ESP_MASTER_DISABLE_CRC, 0);
#endif

#if 0 //Always catch an error: 36
  CEXCEPTION_T err;
  Try {
    ebv_esp_cmd_put_events((uint8_t *)mpack->pack->start, mpack->pack->current - mpack->pack->start, 
                                                                  ARDUINO_ESP_MASTER_DISABLE_CRC, 0);
  } Catch(err) {
    sprintf(_errmsg, "Unexpected ERROR in PUT EVENTS: %d\n", err);
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
    Serial.print(_errmsg);
#endif
    return false;
  }  
#endif                                                     
  if (read_delayed_response_procedure()) {
      return true;
  } else {
        return false;
  }
}


/* DEFINITION OF STATIC FUNCTIONS */

static void wake_esp_slave(void) {
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
  Serial.print("WAKEUP\n");
#endif
  digitalWrite(ARDUINO_ESP_MASTER_WAKE_SLAVE_PIN, LOW);
  delay(1);
  digitalWrite(ARDUINO_ESP_MASTER_WAKE_SLAVE_PIN, HIGH);
}

static void reset_esp_slave(void) {
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
  Serial.print("RESET\n");
#endif
  digitalWrite(ARDUINO_ESP_MASTER_RESET_SLAVE_PIN, LOW);
  delay(1);
  digitalWrite(ARDUINO_ESP_MASTER_RESET_SLAVE_PIN, HIGH);
}

static bool arduino_esp_master_wait_slave_ready_for_cmd(uint32_t timeout) {
  uint32_t timer = 0;
  bool irq;
  irq = !digitalRead(ARDUINO_ESP_MASTER_IRQ_PIN);
  if (irq) {
    return true;
  } else {
    while (!irq) {
      irq = !digitalRead(ARDUINO_ESP_MASTER_IRQ_PIN);
      timer++;
      delay(1);
      if (timer == timeout && !irq) {
        return false;
      } else if (irq) {
        return true;
      }
    }
  }
}

static bool read_delayed_response_procedure(void) {
  CEXCEPTION_T err;
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
  Serial.print("Reading Relayed Response\n");
#endif
  if (arduino_esp_master_wait_slave_ready_for_cmd(ARDUINO_ESP_MASTER_DR_TIMEOUT)) {

#if 1 // Temporary solution due to the issue with error handlers
      // See next "if" condition
    ebv_esp_cmd_read_delayed_resp(&dresp_data, &dresp_length);
#endif

#if 0 //Always catch an error: 2172
    Try {
      ebv_esp_cmd_read_delayed_resp(&dresp_data, &dresp_length);
    } Catch(err) {
      sprintf(_errmsg, "Unexpected ERROR in Read Delayed Response: %d\n", err);
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
      Serial.print(_errmsg);
#endif
      return false;
    }
#endif

#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
    Serial.print("Delayed Response is:\n");
#endif
    printHex_buf(_dresp_data, dresp_length);
  } else {
#ifdef ARDUINO_ESP_MASTER_SERIAL_DEBUG
    Serial.print("ERROR: Delayed Response is not available\n");
#endif
//    Throw(EBV_ESP_UNDEF_ERR);
    return false;
  }
  return true;
}

