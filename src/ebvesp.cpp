/**
 * @brief Implementation of Embever Serial Protocol
 */


#include <stdio.h>

#include "CException.h"

#include "extcwpack.h"
#include "extcwpack_defines.h"

#include "ebvconf.h"
#include "ebvesp.h"
#include "ebvi2c.h"
#include "ebverr.h"
#include "ebvboard.h"
#include "ebvcrc.h"


static uint8_t _ebvesp_master_ready_pin_num;
static uint8_t _ebvesp_slave_ready_pin_num;
static uint8_t _ebvesp_master_irq_pin_num;
static uint8_t _ebvesp_slave_irq_pin_num;

/* TODO: separate slave from master implementation, as they are
 * mutually exclusive. We use the same buffer for both.
 */
static uint8_t _ebv_esp_tx_buf[EBV_CONF_I2C_ESP_TX_BUF_SIZE];
static uint8_t _ebv_esp_rx_buf[EBV_CONF_I2C_ESP_RX_BUF_SIZE];
uint8_t *ebv_esp_tx_buf = (uint8_t *) _ebv_esp_tx_buf;
uint8_t *ebv_esp_rx_buf = (uint8_t *) _ebv_esp_rx_buf;

static struct ebv_i2c_hal_slave_xfer _ebv_esp_scb;
struct ebv_i2c_hal_slave_xfer *ebv_esp_scb = &_ebv_esp_scb;

static struct ebv_i2c _ebv_i2c_esp;
struct ebv_i2c *ebv_i2c_esp = &_ebv_i2c_esp;

volatile uint8_t ready_for_rdr_cmd;
uint8_t ebv_esp_tx_data_pending_buf[EBV_CONF_I2C_ESP_SLAVE_TX_BUF_SIZE
        - EBVESP_I2C_OVERHEAD_HEADER];
uint8_t ebv_esp_header_buf[EBVESP_I2C_OVERHEAD_HEADER];
volatile uint8_t cmd_pending = EBVESP_I2C_CMD_NO_COMMAND;
uint8_t *_ebv_esp_data;
uint16_t _ebv_esp_datalen;
volatile uint16_t data_pending_len = 0;
static uint16_t data_sent_cntr = 0;

static void ebv_esp_reset_slave_delayed_response(void);

/*------------------------------------------------------------------------------
 * Definition of slave callback functions
 *----------------------------------------------------------------------------*/

uint16_t call_num_scb_send = 0;
uint16_t call_num_scb_recv = 0;
uint16_t addr_scb_start = 0;

static uint16_t call_num_scb_send_old;
static uint16_t call_num_scb_recv_old;
volatile static uint16_t counter;

static bool sending_delayed_response_packet = false;

/* Static functions */

/**
 * @brief Transmit and receive ESP master data via I2C
 *
 * Particular limitations of the I2C slave HW driver used by ESP slave
 * are considered by this functions (like delays).
 */
static void ebv_esp_master_txrx_request(uint8_t *txbuf, uint16_t txlen,
		uint8_t *rxbuf, uint16_t rxlen);

/**
 * @brief I2C slave service start callback
 *
 * This callback is called from the I2C slave handler when an I2C slave address
 * is received and needs servicing. It's used to indicate the start of a slave
 * transfer that will happen on the slave bus.
 */
static void ebv_esp_slave_start(uint8_t addr)
{
    call_num_scb_send_old = call_num_scb_send;
    call_num_scb_recv_old = call_num_scb_recv;
    addr_scb_start = addr;
    counter = 0;

    return;
}

/**
 * @brief I2C slave send data callback
 *
 * This callback is called from the I2C slave handler when an I2C slave address
 * needs data to send. Return non-0 value to NACK the master and terminate the
 * transfer, or return 0 to continue transfer with 1 byte.
 */
static uint8_t ebv_esp_slave_send(uint8_t * txbyte)
{
    uint8_t done = 0;

    if (sending_delayed_response_packet) {
        *txbyte = ebv_i2c_esp->txbuf[counter + data_sent_cntr];
    }
    else {
        *txbyte = ebv_i2c_esp->txbuf[counter];
    }
    counter++;

    if (ebv_i2c_esp->txlen == counter) {
        done = 1;
    }
    call_num_scb_send++;

    return done;
}

/**
 * @brief I2C slave receive data callback
 *
 * This callback is called from the I2C slave handler when an I2C slave
 * address has receive data. Return non-0 value to NACK the master and terminate
 * the transfer, or return 0 to continue the transfer.
 */
static uint8_t ebv_esp_slave_recv(uint8_t rxbyte)
{
    uint8_t done = 0;
    ebv_i2c_esp->rxbuf[counter] = rxbyte;

    counter++;
    if (ebv_i2c_esp->rxlen == counter) {
        done = 1;
    }
    call_num_scb_recv++;

    return done;
}

/**
 * @brief I2C slave service done callback
 *
 * This callback is called from the I2C slave handler when an I2C slave transfer
 * is completed. It's used to indicate the end of a slave transfer.
 */
static void ebv_esp_slave_done(void)
{
	volatile static uint16_t err_code = 0;
    uint16_t num_bytes;
    uint8_t cmd_id = ebv_i2c_esp->rxbuf[0];

    /* slave received data */
    if (call_num_scb_recv_old != call_num_scb_recv) {

        num_bytes = call_num_scb_recv - call_num_scb_recv_old;

        switch (cmd_id) {
        case EBVESP_I2C_CMD_READ_DELAYED_RESP:
            if (false == ebv_digital_read(_ebvesp_slave_irq_pin_num)) {
                /* success, responses */
                sending_delayed_response_packet =
                        ebv_esp_write_response_packet_to_tx_buf(data_pending_len);
            } else {
                /* success, no responses */
                ebv_esp_build_response_packet_header(
                        EBVESP_I2C_CMD_READ_DELAYED_RESP, 0);
            }
            break;
        case EBVESP_I2C_CMD_GET_ACTIONS:
        	ebv_esp_reset_slave_delayed_response();
            ebv_esp_ack_packet(ebv_i2c_esp->rxbuf);
            cmd_pending = EBVESP_I2C_CMD_GET_ACTIONS;
            break;
        case EBVESP_I2C_CMD_PUT_RESULTS:
        case EBVESP_I2C_CMD_PUT_EVENTS:
        case EBVESP_I2C_CMD_PERFORM_ACTIONS:
        case EBVESP_I2C_CMD_READ_LOCAL_FILE:
        	ebv_esp_reset_slave_delayed_response();
			ebv_esp_ack_packet(ebv_i2c_esp->rxbuf);
			cmd_pending = cmd_id;
			err_code = ebv_esp_eval_command_packet(num_bytes);
			if (err_code != 0) {
				data_pending_len = ebv_esp_copy_response_packet_error_to_data_pending_buf(
						cmd_id, err_code);
			}
            break;
        default:
			ebv_esp_ack_packet(ebv_i2c_esp->rxbuf);
			data_pending_len = ebv_esp_copy_response_packet_error_to_data_pending_buf(
						cmd_id, ESP_ERR_INVALID_CMD_ID);
            break;
        }
    }

    /* slave sent data */
    if (call_num_scb_send_old != call_num_scb_send) {

        num_bytes = call_num_scb_send - call_num_scb_send_old;

        if (sending_delayed_response_packet) {
            data_sent_cntr += num_bytes;

            if ((EBVESP_I2C_OVERHEAD_HEADER + data_pending_len)
                    <= data_sent_cntr) {
            	ebv_esp_reset_slave_delayed_response();
            }
        }
    }

    counter = 0;
    return;
}

static void ebv_esp_reset_slave_delayed_response(void)
{
	sending_delayed_response_packet = false;
	cmd_pending = EBVESP_I2C_CMD_NO_COMMAND;
	data_sent_cntr = 0;
	ebv_digital_write(_ebvesp_slave_irq_pin_num, true);
	return;
}


/*****************************   M A S T E R   ********************************/
void ebv_esp_init_master(uint8_t irqpin, uint8_t readypin)
{
    _ebvesp_master_irq_pin_num = irqpin;
    _ebvesp_master_ready_pin_num = readypin;
	/* Check that txlen and rxlen have the right size */
    ebv_i2c_set_iface_role(EBV_I2C_HAL_IFACE_I2C0, EBV_I2C_IFACE_ROLE_MASTER);
    ebv_i2c_esp->id = EBV_I2C_HAL_IFACE_I2C0;
    ebv_i2c_esp->txbuf = (uint8_t *)_ebv_esp_tx_buf;
    ebv_i2c_esp->txlen = sizeof(_ebv_esp_tx_buf);
    ebv_i2c_esp->rxbuf = (uint8_t *)_ebv_esp_rx_buf;
    ebv_i2c_esp->rxlen = sizeof(_ebv_esp_rx_buf);
}

bool ebv_esp_is_interface_ready(void)
{
	return !ebv_digital_read(_ebvesp_master_ready_pin_num);  /* active low */
}

void ebv_esp_is_ready_for_rdr_cmd(uint32_t timeout)
{
    ready_for_rdr_cmd = ebv_board_waiting_for_esp_irq_pin_goes_low(
            _ebvesp_master_irq_pin_num, timeout);

    if (0 == ready_for_rdr_cmd) {
        Throw(EBVESP_I2C_RESPONSE_TIMEOUT);
    }

    return;
}

void ebv_esp_cmd_put_events(uint8_t *data, uint16_t data_len,
        uint8_t disable_crc, ebv_qos_levels qos)
{
    uint8_t cmd = EBVESP_I2C_CMD_PUT_EVENTS;

    ebv_esp_send_command_packet_with_data(cmd, data, data_len, disable_crc, qos);

    return;
}

void ebv_esp_cmd_read_local_file(uint8_t *data, uint16_t data_len,
        uint8_t disable_crc, ebv_qos_levels qos)
{
    uint8_t cmd = EBVESP_I2C_CMD_READ_LOCAL_FILE;

    ebv_esp_send_command_packet_with_data(cmd, data, data_len, disable_crc, qos);

    return;
}

void ebv_esp_cmd_perform_actions(uint8_t *data, uint16_t data_len,
        uint8_t disable_crc, ebv_qos_levels qos)
{
    uint8_t cmd = EBVESP_I2C_CMD_PERFORM_ACTIONS;

    ebv_esp_send_command_packet_with_data(cmd, data, data_len, disable_crc, qos);
    // printHex_buf(data, data_len);

    return;
}


bool ebv_esp_cmd_read_delayed_resp(uint8_t **resp_pkt_data, uint16_t *resp_pkt_data_len)
{   


    // Serial.print("\n\nInside RDR\n");




	bool is_delayed_resp;
    uint8_t read_delayed_resp_cmd = EBVESP_I2C_CMD_READ_DELAYED_RESP;
    uint16_t response_len;
    uint8_t *rx_delayed_response = ebv_i2c_esp->rxbuf;
    /* Invalidate this pointers in case an error occur before they are set */
	*resp_pkt_data = NULL;
	*resp_pkt_data_len = 0;

    /* Read the first 4 bytes of response packet (success, responses) of
     * 'Read Delayed Response' command (0xA1) and get the length of response */
    response_len = ebv_esp_tx_cmd_rx_response_packet(read_delayed_resp_cmd);


    // Serial.print(response_len);
    // Serial.print("\n");


    if (0 != response_len) {    /* Response pkt with data (success, responses) */
#if 0
        /* Prepared for using with a 'response data' bigger than rx buffer length.
         * But first we must agree on where will this data stored.
         * It will be just overwritten in this code snippet.
         * By now we just checkin the data length and throw an error if the data
         * does not fit into the rx buf.
         */
        while( response_len > ebv_i2c_esp->rxlen ) { /* get the response data in full buffer chunks */
            ebv_i2c_master_txrx_request(ebv_i2c_esp, EBVESP_I2C_SLAVE_ADDR,
                    0, 0, rx_delayed_response, ebv_i2c_esp->rxlen);
            response_len -= ebv_i2c_esp->rxlen;
        }
#endif

    	is_delayed_resp = true;
        /* Check if the response pkt fit into the Rx buf and than get this data*/
        ebv_esp_eval_response_data_len(response_len);
        ebv_esp_master_txrx_request(NULL, 0, rx_delayed_response, response_len);

        /* check response packet command id */
        uint8_t cmd_id = rx_delayed_response[1];
        /* and the length of the data, if any */
        uint16_t data_len = ( ((uint16_t)rx_delayed_response[3] << 8)
               | rx_delayed_response[2] );

        switch (cmd_id) {
        case EBVESP_I2C_CMD_GET_ACTIONS:
        case EBVESP_I2C_CMD_READ_LOCAL_FILE:
        case EBVESP_I2C_CMD_PERFORM_ACTIONS:
        	if (0 != data_len) {
				ebv_esp_eval_response_packet_with_data(response_len);
        	}
            break;
        case EBVESP_I2C_CMD_PUT_RESULTS:
        case EBVESP_I2C_CMD_PUT_EVENTS:
            break;
        default:
            Throw(ESP_ERR_INVALID_RESP_CMD_ID);
            break;
        }
        /* If no data or error, the data pointer is invalid */
        if (0 == data_len) {
        	*resp_pkt_data = NULL;
			*resp_pkt_data_len = 0;
        } else if (EBVESP_I2C_LEN_ON_ERROR == data_len) {
			*resp_pkt_data = NULL;
			*resp_pkt_data_len = 0;
            Throw(EBVESP_I2C_RESPONSE_PKT_CONTAINS_ERR_CODE);
        } else {
			*resp_pkt_data_len = (data_len - (uint16_t)EBVESP_I2C_OVERHEAD_CSUM_FLAGS);
			*resp_pkt_data = &rx_delayed_response[EBVESP_I2C_OVERHEAD_HEADER];
        }

    } else {
    	is_delayed_resp = false;
        *resp_pkt_data = NULL;
        *resp_pkt_data_len = 0;
    }

    ready_for_rdr_cmd = 0;

    return is_delayed_resp;
}

uint16_t ebv_esp_tx_cmd_rx_response_packet(uint8_t cmd)
{
    uint8_t rx_sor[EBVESP_I2C_OVERHEAD_HEADER];
    const uint8_t sor = EBVESP_I2C_SOP_SOR_ID;

    ebv_esp_master_txrx_request(&cmd, 1, rx_sor, EBVESP_I2C_OVERHEAD_HEADER);
    if (sor != rx_sor[0]) {
        Throw(EBVESP_I2C_FIELD_SOP_ERR);
    }
    if (cmd != rx_sor[1]) {
        Throw(EBVESP_I2C_FIELD_COMMAND_ERR);
    }

    uint16_t response_length = ((uint16_t) rx_sor[3] << 8) | rx_sor[2];

    return response_length;
}

void ebv_esp_eval_response_data_len(uint16_t response_length)
{
    if (response_length > ebv_i2c_esp->rxlen) {
        Throw(EBV_I2C_BUFSIZE_ERR);
    }

    return;
}

uint8_t ebv_esp_build_flags_field(bool ignore_crc, ebv_qos_levels qos)
{
    uint8_t flags = 0;

    if ((qos >= EBVESP_QOS_0) && (qos <= EBVESP_QOS_3)) {
        flags = qos << 1;
    } else {
        Throw(EBVESP_I2C_FLAGS_FIELD_QOS_ERR);
    }

    if (true == ignore_crc) {
        flags |= (1 << 0);
    } else {
        flags &= ~(1 << 0);
    }

    return flags;
}

void ebv_esp_cmd_get_actions(void)
{
    uint8_t cmd_packet[] = {EBVESP_I2C_CMD_GET_ACTIONS};
    uint16_t cmd_packet_len = 1;

    ebv_esp_tx_cmd_rx_ack_and_check(cmd_packet, cmd_packet_len);

    return;
}

void ebv_esp_tx_cmd_rx_ack_and_check(uint8_t *cmd_packet, uint16_t cmd_packet_len)
{
    uint8_t rx_ack_packet[2];
    const uint8_t sop = EBVESP_I2C_SOP_SOA_ID;
    uint8_t ack = cmd_packet[0];

    if (cmd_packet_len > ebv_i2c_esp->txlen) {
        Throw(EBV_I2C_BUFSIZE_ERR);
    }

    ebv_esp_master_txrx_request(cmd_packet, cmd_packet_len,
    		rx_ack_packet, sizeof(rx_ack_packet));

    if (sop != rx_ack_packet[0]) {
        Throw(EBVESP_I2C_FIELD_SOP_ERR);
    }
    if (ack != rx_ack_packet[1]) {
        Throw(EBVESP_I2C_FIELD_ACK_ERR);
    }

    return;
}

void ebv_esp_cmd_put_results(uint8_t *data, uint16_t data_len,
        uint8_t disable_crc, ebv_qos_levels qos)
{
    uint8_t cmd = EBVESP_I2C_CMD_PUT_RESULTS;

    ebv_esp_send_command_packet_with_data(cmd, data, data_len, disable_crc, qos);

    return;
}

void ebv_esp_send_command_packet_with_data(uint8_t cmd, uint8_t *data,
        uint16_t data_len, uint8_t disable_crc, ebv_qos_levels qos)
{
    uint8_t checksum[] = {0x00, 0x00, 0x00, 0x00}; /* If disable_crc == 1 */
    uint8_t *cmd_pkt = ebv_i2c_esp->txbuf;
    uint16_t cmd_pkt_len;
    uint16_t length; /* Combined length of data and crc (data still to read) */
    uint8_t flags;

    length = data_len + EBVESP_I2C_OVERHEAD_CRC;
    flags = ebv_esp_build_flags_field((bool)disable_crc, qos);

    /* Build a command packet */
    memcpy(&cmd_pkt[0], &cmd, sizeof(cmd));
    memcpy(&cmd_pkt[1], &flags, sizeof(flags));   /* CRC and QoS */
    memcpy(&cmd_pkt[2], &length, sizeof(length)); /* Little-endian format */
    memcpy(&cmd_pkt[EBVESP_I2C_OVERHEAD_HEADER], data, data_len);

    if (false == disable_crc) {
        /* compute CRC32 */
        uint32_t crc = ebv_crc_compute_crc32((uint8_t *)data, data_len);
        memcpy(checksum, &crc, sizeof(checksum));
    }
    memcpy(&cmd_pkt[EBVESP_I2C_OVERHEAD_HEADER + data_len], checksum,
            sizeof(checksum));

    cmd_pkt_len = EBVESP_I2C_OVERHEAD_HEADER + length;

    /* Send a command packet */
    ebv_esp_tx_cmd_rx_ack_and_check(cmd_pkt, cmd_pkt_len);

    return;
}

void ebv_esp_eval_response_packet_with_data(uint16_t rx_bytes)
{
    uint8_t *rxbuf = ebv_i2c_esp->rxbuf;
    int crc_ok = 0;
    uint32_t crc;
    uint16_t overhead_total = EBVESP_I2C_OVERHEAD_HEADER
            + EBVESP_I2C_OVERHEAD_CSUM_FLAGS;

    /* 1st check */
    if (overhead_total >= rx_bytes) {
        Throw(ESP_ERR_INVALID_RESP_DATA);
    }

    uint16_t data_len = rx_bytes - overhead_total;
    uint8_t data[data_len];
    uint8_t crc_rx[EBVESP_I2C_OVERHEAD_CRC];

    memcpy(&data[0], &rxbuf[EBVESP_I2C_OVERHEAD_HEADER], data_len);
    memcpy(&crc_rx, &rxbuf[EBVESP_I2C_OVERHEAD_HEADER + data_len],
            EBVESP_I2C_OVERHEAD_CRC);

    uint8_t flags = rxbuf[EBVESP_I2C_OVERHEAD_HEADER + data_len
            + EBVESP_I2C_OVERHEAD_CRC];
    bool ignore_crc = (bool)(flags & 0x01);

    /* compute CRC32 */
    if (false == ignore_crc) {
        crc = ebv_crc_compute_crc32((uint8_t *)data, data_len);
        crc_ok = memcmp(&crc_rx, &crc, EBVESP_I2C_OVERHEAD_CRC);
    }

    /* 2nd check */
    if (0 != crc_ok) {
        Throw(ESP_ERR_INVALID_RESP_DATA);
    }

    return;
}


/******************************   S L A V E   *********************************/

void ebv_esp_init_slave(uint8_t irqpin, uint8_t readypin)
{
    _ebvesp_slave_irq_pin_num = irqpin;
    _ebvesp_slave_ready_pin_num = readypin;

    ebv_i2c_set_iface_role(EBV_I2C_HAL_IFACE_I2C0, EBVESP_I2C_SLAVE_ADDR);
    ebv_i2c_esp->id = EBV_I2C_HAL_IFACE_I2C0;
    ebv_i2c_esp->txbuf = (uint8_t *)_ebv_esp_tx_buf;
    ebv_i2c_esp->txlen = sizeof(_ebv_esp_tx_buf);
    ebv_i2c_esp->rxbuf = (uint8_t *)_ebv_esp_rx_buf;
    ebv_i2c_esp->rxlen = sizeof(_ebv_esp_rx_buf);

    ebv_esp_scb->start = ebv_esp_slave_start;
    ebv_esp_scb->send = ebv_esp_slave_send;
    ebv_esp_scb->recv = ebv_esp_slave_recv;
    ebv_esp_scb->done = ebv_esp_slave_done;
    return;
}

void ebv_esp_start_slave(void)
{
    ebv_i2c_start_slave(ebv_i2c_esp, ebv_esp_scb);
	ebv_digital_write(_ebvesp_slave_irq_pin_num, true);
    ebv_digital_write(_ebvesp_slave_ready_pin_num, false);
    return;
}

void ebv_esp_stop_slave(void)
{
    ebv_i2c_stop_slave(ebv_i2c_esp);
    return;
}

void ebv_esp_ack_packet(uint8_t *cmd_packet)
{
    const uint8_t sop = EBVESP_I2C_SOP_SOA_ID;

    /* Build an ack packet. It will be send by ebv_esp_slave_send() callback */
    ebv_i2c_esp->txbuf[0] = sop;
    ebv_i2c_esp->txbuf[1] = cmd_packet[0];

    return;
}

void ebv_esp_build_response_packet_header(uint8_t cmd, uint16_t resp_len)
{
    const uint8_t sop = EBVESP_I2C_SOP_SOR_ID;

    ebv_esp_header_buf[0] = sop;
    memcpy(&ebv_esp_header_buf[1], &cmd, sizeof(cmd));
    memcpy(&ebv_esp_header_buf[2], &resp_len, sizeof(resp_len));

    return;
}

void ebv_esp_copy_response_packet_header_to_buf(uint8_t *target_buf)
{
    memcpy(target_buf, ebv_esp_header_buf, sizeof(ebv_esp_header_buf));

    return;
}

uint16_t ebv_esp_copy_response_w_actions_data_to_data_pending_buf(uint8_t *data,
        uint16_t data_len, bool ignore_crc, ebv_qos_levels qos)
{
    uint8_t checksum[] = {0x00, 0x00, 0x00, 0x00};
    uint8_t flags = ebv_esp_build_flags_field(ignore_crc, qos);
    uint16_t data_len_w_overhead = data_len + EBVESP_I2C_OVERHEAD_CSUM_FLAGS;

    if ((uint16_t)(ebv_i2c_esp->txlen - EBVESP_I2C_OVERHEAD_HEADER)
            < data_len_w_overhead) {
        Throw(EBV_I2C_BUFSIZE_ERR);
    }

    /* compute CRC32 */
    if (false == ignore_crc) {
        uint32_t crc = ebv_crc_compute_crc32((uint8_t *)data, data_len);
        memcpy(checksum, &crc, sizeof(checksum));
    }

    /* build response header */
    ebv_esp_build_response_packet_header(cmd_pending, data_len_w_overhead);
    ebv_esp_copy_response_packet_header_to_buf(ebv_esp_tx_data_pending_buf);

    /* build response 'payload' */
    memcpy(&ebv_esp_tx_data_pending_buf[EBVESP_I2C_OVERHEAD_HEADER], data,
            data_len);
    memcpy(&ebv_esp_tx_data_pending_buf[EBVESP_I2C_OVERHEAD_HEADER + data_len],
            &checksum, EBVESP_I2C_OVERHEAD_CRC);
    memcpy(&ebv_esp_tx_data_pending_buf[EBVESP_I2C_OVERHEAD_HEADER + data_len
                    + EBVESP_I2C_OVERHEAD_CRC], &flags, sizeof(flags));

     ebv_digital_write(_ebvesp_slave_irq_pin_num, false);

    return (EBVESP_I2C_OVERHEAD_HEADER + data_len_w_overhead);
}

uint16_t ebv_esp_copy_response_w_no_data_to_data_pending_buf(void)
{
    ebv_esp_build_response_packet_header(cmd_pending, 0);
    ebv_esp_copy_response_packet_header_to_buf(ebv_esp_tx_data_pending_buf);

    ebv_digital_write(_ebvesp_slave_irq_pin_num, false);

    return EBVESP_I2C_OVERHEAD_HEADER;
}

uint16_t ebv_esp_copy_response_packet_error_to_data_pending_buf(uint8_t cmd, uint16_t err)
{
    const uint16_t error_code_len = 4;
    const uint16_t err_const = 0xB3A5;  /* 0xA5 is the less significant byte */

    /* build a response packet header with an error */
    ebv_esp_build_response_packet_header(cmd, error_code_len);
    ebv_esp_copy_response_packet_header_to_buf(ebv_esp_tx_data_pending_buf);

    /* build an error code */
    memcpy(&ebv_esp_tx_data_pending_buf[EBVESP_I2C_OVERHEAD_HEADER],
            &err_const, sizeof(uint16_t));
    memcpy(&ebv_esp_tx_data_pending_buf[EBVESP_I2C_OVERHEAD_HEADER + sizeof(uint16_t)],
            &err, sizeof(uint16_t));

    ebv_digital_write(_ebvesp_slave_irq_pin_num, false);

    return (EBVESP_I2C_OVERHEAD_HEADER + error_code_len);
}

bool ebv_esp_write_response_packet_to_tx_buf(uint16_t response_len)
{
    const uint8_t cmd = EBVESP_I2C_CMD_READ_DELAYED_RESP;

    /* header of the READ_DELAYED_RESP command */
    ebv_esp_build_response_packet_header(cmd, response_len);
    ebv_esp_copy_response_packet_header_to_buf(ebv_i2c_esp->txbuf);

    /* The delayed response packet */
    memcpy(&ebv_i2c_esp->txbuf[EBVESP_I2C_OVERHEAD_HEADER],
            ebv_esp_tx_data_pending_buf, response_len);

    if (EBVESP_I2C_CMD_GET_ACTIONS == cmd_pending) {

        if ((uint16_t)(EBVESP_I2C_OVERHEAD_CSUM_FLAGS
                + EBVESP_I2C_OVERHEAD_HEADER + 1) > response_len) {
            Throw(EBVESP_I2C_RESPONSE_LEN_ERR);
        }
    }

    return true;
}

uint16_t ebv_esp_eval_command_packet(uint16_t rx_bytes)
{
    uint8_t *rxbuf = ebv_i2c_esp->rxbuf;
    uint8_t flags = rxbuf[1];
    uint16_t len_field = ((uint16_t) rxbuf[3] << 8) | rxbuf[2];
    uint16_t data_len = len_field - EBVESP_I2C_OVERHEAD_CRC;
    uint8_t data[len_field];
    uint32_t crc;
    uint8_t crc_rx[EBVESP_I2C_OVERHEAD_CRC];
    int crc_ok = 0; /* is needed to bypass the if operator if crc is disabled */
    uint16_t err_code = 0;

    memcpy(&data, &rxbuf[EBVESP_I2C_OVERHEAD_HEADER], data_len);
    memcpy(&crc_rx, &rxbuf[EBVESP_I2C_OVERHEAD_HEADER + data_len],
            EBVESP_I2C_OVERHEAD_CRC);

    bool ignore_crc = (bool)(flags & 0x01);

    /* compute CRC32 */
    if (false == ignore_crc) {
        crc = ebv_crc_compute_crc32((uint8_t *)data, data_len);
        crc_ok = memcmp(&crc_rx, &crc, EBVESP_I2C_OVERHEAD_CRC);
    }

    /* build a response packet header with an INVALID_CMD_DATA error */
    if (((len_field + EBVESP_I2C_OVERHEAD_HEADER) != rx_bytes)
            || (0 != crc_ok)) {
    	err_code = ESP_ERR_INVALID_CMD_DATA;
    	_ebv_esp_data = NULL;
    	_ebv_esp_datalen = 0;
    } else {
    	_ebv_esp_data = &rxbuf[EBVESP_I2C_OVERHEAD_HEADER];
    	_ebv_esp_datalen = data_len;
    }

    return err_code;
}

/* Definition of static functions */

static void ebv_esp_master_txrx_request(uint8_t *txbuf, uint16_t txlen,
		uint8_t *rxbuf, uint16_t rxlen)
{
    if (txlen != 0) {
        ebv_i2c_master_txrx_request(ebv_i2c_esp, EBVESP_I2C_SLAVE_ADDR,
                txbuf, txlen, NULL, 0);
        ebv_delay(1);
    }

    if (rxlen != 0) {
        ebv_delay(1);
        ebv_i2c_master_txrx_request(ebv_i2c_esp, EBVESP_I2C_SLAVE_ADDR,
                NULL, 0, rxbuf, rxlen);
    }
    return;
}
