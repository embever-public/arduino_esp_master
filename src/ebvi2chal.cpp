#include <Wire.h>

#include <ebvi2chal.h>


void ebv_i2c_hal_setup_master(ebv_i2c_hal_iface_id_t id)
{
	Wire.begin();
}

void ebv_i2c_hal_master_txrx_request(const ebv_i2c_hal_iface_id_t id,
                                     const uint8_t slaveaddr,
                                     uint8_t *txbuf, uint16_t txlen,
                                     uint8_t *rxbuf, uint16_t rxlen)
{
	if (txlen > 0) {
		Wire.beginTransmission(slaveaddr);  /* might need to shift this? */
		Wire.write(txbuf, txlen);
		Wire.endTransmission();
	}
	if (rxlen > 0) {
		Wire.requestFrom(slaveaddr, rxlen);  /* might need to shift this? */
		int i = 0;
		while(Wire.available())    // slave may send less than requested
		{
			rxbuf[i] = (uint8_t) Wire.read();    // receive a byte as character
			i++;
			if (i >= rxlen) {
				break;
			}
			//Serial.print(c);         // print the character
		}
	}
}
