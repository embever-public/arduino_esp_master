#include <Arduino.h>

#include <ebvboard.h> 


bool ebv_digital_read(uint8_t pin)
{
	return digitalRead(pin);
}

void ebv_delay(uint32_t t_ms)
{
	delay(t_ms);
}
