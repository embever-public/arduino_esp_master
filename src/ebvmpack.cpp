/*
 * ebvmpack.c
 *
 *  Created on: 6 Feb 2018
 *      Author: pablo
 */
#include <string.h>

#include "CException.h"

#include "extcwpack.h"
#include "ebverr.h"
#include "ebvmpack.h"

static cw_unpack_context _ebv_mpack_cwpack_uc;
static cw_pack_context _ebv_mpack_cwpack_pc;
static struct ebv_mpack_requests_object _ebv_mpack_payload_obj;
static struct ebv_mpack_requests_payload _ebv_mpack_payload;

void ebv_mpack_init_context(struct ebv_mpack *mpack,
		void *rxbuf, uint32_t rxbuflen,
		void *txbuf, uint32_t txbuflen,
		cw_unpack_context *unpack, cw_pack_context *pack)
{
    cw_unpack_context_init(unpack, rxbuf, rxbuflen, 0);
    cw_pack_context_init(pack, txbuf, txbuflen, 0);
	/* Make strings longer than 31 chars to be serialized like Python msgpack */
	cw_pack_set_compatibility(pack, true);
    mpack->unpack = unpack;
    mpack->pack = pack;
    mpack->payload = &_ebv_mpack_payload;
    mpack->payload->obj = &_ebv_mpack_payload_obj;
	return;
}

void ebv_mpack_init(struct ebv_mpack *mpack,
		void *rxbuf, uint32_t rxbuflen,
		void *txbuf, uint32_t txbuflen)
{
    cw_unpack_context_init(&_ebv_mpack_cwpack_uc, rxbuf, rxbuflen, 0);
    cw_pack_context_init(&_ebv_mpack_cwpack_pc, txbuf, txbuflen, 0);
	/* Make strings longer than 31 chars to be serialized like Python msgpack */
	cw_pack_set_compatibility (&_ebv_mpack_cwpack_pc, true);
    mpack->unpack = &_ebv_mpack_cwpack_uc;
    mpack->pack = &_ebv_mpack_cwpack_pc;
    mpack->payload = &_ebv_mpack_payload;
    mpack->payload->obj = &_ebv_mpack_payload_obj;
	return;
}

/* Emvbever IoT Protocol v2.0 */

void ebv_mpack_get_msg_type(struct ebv_mpack *mpack)
{
	uint64_t type;
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_ARRAY) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_POSITIVE_INTEGER) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	type = (uint64_t)mpack->unpack->item.as.u64;

	switch(type) {
	case(EBV_MPACK_MSG_TYPE_DISCONNECT):
	case(EBV_MPACK_MSG_TYPE_ACTIONS):
		{
		mpack->msg_type = type;
	}
	break;
	default:
		mpack->msg_type = EBV_MPACK_MSG_TYPE_UNKNOWN;
	}
	return;
}

void ebv_mpack_get_actions_list(struct ebv_mpack *mpack)
{
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_ARRAY) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	mpack->payload->num = mpack->unpack->item.as.array.size;
	return;
}

void ebv_mpack_get_next_private_results_list(struct ebv_mpack *mpack,
		struct ebv_mpack_requests_object *obj)
{
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_ARRAY) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	/* EBV IOT Prot. V2.1, 3 elements: ID, Object and result code */
	if (3 != mpack->unpack->item.as.array.size) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	/* id */
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_POSITIVE_INTEGER) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	if (mpack->payload->obj->id != mpack->unpack->item.as.u64) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	/* The object */
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_MAP) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	/* type */
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_STR) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	obj->type.start = mpack->unpack->item.as.str.start;
	obj->type.length = mpack->unpack->item.as.str.length;

	return;
}

void ebv_mpack_build_results_message_start(struct ebv_mpack *mpack)
{
	cw_pack_array_size(mpack->pack, 3);
	cw_pack_unsigned(mpack->pack, EBV_MPACK_MSG_TYPE_RESULTS);
	return;
}

void ebv_mpack_build_private_results_start(struct ebv_mpack *mpack)
{
	cw_pack_array_size(mpack->pack, mpack->payload->num);
	return;
}

void ebv_mpack_build_public_results_start(struct ebv_mpack *mpack)
{
	ebv_mpack_build_private_results_start(mpack);
	return;
}

void ebv_mpack_build_private_events_start(struct ebv_mpack *mpack)
{
	cw_pack_array_size(mpack->pack, 3);
	cw_pack_unsigned(mpack->pack, EBV_MPACK_MSG_TYPE_EVENTS);
	cw_pack_array_size(mpack->pack, mpack->payload->num);
	return;
}

void ebv_mpack_get_next_actions_object(struct ebv_mpack *mpack,
		struct ebv_mpack_requests_object *obj)
{
	uint32_t array_size;
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_ARRAY) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	array_size = mpack->unpack->item.as.array.size;
	if (array_size < 3) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	/* type */
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_STR) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	obj->type.start = mpack->unpack->item.as.str.start;
	obj->type.length = mpack->unpack->item.as.str.length;

	/* id */
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_POSITIVE_INTEGER) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	obj->id = (uint64_t)mpack->unpack->item.as.u64;
	/* data */
	cw_unpack_next(mpack->unpack);
	if ((mpack->unpack->item.type != CWP_ITEM_MAP) &&
			(mpack->unpack->item.type != CWP_ITEM_NIL))
	{
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	return;
}

void ebv_mpack_set_next_events_object(struct ebv_mpack *mpack,
		struct ebv_mpack_requests_object *obj)
{
	cw_pack_array_size(mpack->pack, 2);
	ebv_mpack_add_str(mpack, "_wakeUp");  /* Quick and dirty: Fixed string */
	return;
}

void ebv_mpack_set_next_results_object(struct ebv_mpack *mpack,
		struct ebv_mpack_requests_object *obj, uint32_t length)
{
	cw_pack_array_size(mpack->pack, length);
	cw_pack_unsigned(mpack->pack, obj->id);
	return;
}

/* Emvbever IoT Protocol v1.x */

void ebv_mpack_get_action_type(struct ebv_mpack *mpack)
{
	cwpack_blob *str;
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_MAP) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_STR) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	str = &(mpack->unpack->item.as.str);
	if (ebv_mpack_strcmp("type", str) != 0) {
		Throw(EBV_MPACK_REQUEST_KEY_ERR);
	}
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_STR) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	str = &(mpack->unpack->item.as.str);
	if (ebv_mpack_strcmp("connect", str) == 0) {
		mpack->type = EBV_MPACK_ACTION_TYPE_CONNECT;
	} else if (ebv_mpack_strcmp("disconnect", str)  == 0) {
		mpack->type = EBV_MPACK_ACTION_TYPE_DISCONNECT;
	} else if (ebv_mpack_strcmp("requests", str)  == 0) {
		mpack->type = EBV_MPACK_ACTION_TYPE_REQUESTS;
	} else {
		mpack->type = EBV_MPACK_ACTION_TYPE_UNKNOWN;
	}

	return;
}

void ebv_mpack_get_requests_objects_list(struct ebv_mpack *mpack)
{
	cwpack_blob *str;
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_STR) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	str = &(mpack->unpack->item.as.str);
	if (ebv_mpack_strcmp("payload", str) != 0) {
		Throw(EBV_MPACK_REQUEST_KEY_ERR);
	}
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_ARRAY) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	mpack->payload->num = mpack->unpack->item.as.array.size;
	return;
}

void ebv_mpack_build_responses_start(struct ebv_mpack *mpack)
{
	cw_pack_map_size(mpack->pack, 2);
	ebv_mpack_add_str(mpack, "type");
	ebv_mpack_add_str(mpack, "responses");
	ebv_mpack_add_str(mpack, "payload");
	cw_pack_array_size(mpack->pack, mpack->payload->num);
	return;
}

void ebv_mpack_build_events_start(struct ebv_mpack *mpack)
{
	cw_pack_map_size(mpack->pack, 2);
	ebv_mpack_add_str(mpack, "type");
	ebv_mpack_add_str(mpack, "events");
	ebv_mpack_add_str(mpack, "payload");
	cw_pack_array_size(mpack->pack, mpack->payload->num);
	return;
}

void ebv_mpack_get_next_requests_object(struct ebv_mpack *mpack, struct ebv_mpack_requests_object *obj)
{
	cwpack_blob *str;
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_MAP) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	/* type */
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_STR) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	str = &(mpack->unpack->item.as.str);
	if (ebv_mpack_strcmp("type", str) != 0) {
		Throw(EBV_MPACK_REQUEST_KEY_ERR);
	}
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_STR) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	obj->type.start = mpack->unpack->item.as.str.start;
	obj->type.length = mpack->unpack->item.as.str.length;

	/* id */
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_STR) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	str = &(mpack->unpack->item.as.str);
	if (ebv_mpack_strcmp("id", str) != 0) {
		Throw(EBV_MPACK_REQUEST_KEY_ERR);
	}
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_POSITIVE_INTEGER) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	obj->id = (uint64_t)mpack->unpack->item.as.u64;
	/* data */
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_STR) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	str = &(mpack->unpack->item.as.str);
	if (ebv_mpack_strcmp("data", str) != 0) {
		Throw(EBV_MPACK_REQUEST_KEY_ERR);
	}
	cw_unpack_next(mpack->unpack);
	if ((mpack->unpack->item.type != CWP_ITEM_MAP) &&
			(mpack->unpack->item.type != CWP_ITEM_NIL))
	{
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	return;
}

void ebv_mpack_set_next_responses_object(struct ebv_mpack *mpack, struct ebv_mpack_requests_object *obj)
{
	cw_pack_map_size(mpack->pack, 2);
	ebv_mpack_add_str(mpack, "id");
	cw_pack_unsigned(mpack->pack, obj->id);
	ebv_mpack_add_str(mpack, "data");
	return;
}

/* Functions was renamed with suffix v10 to make clear is for
 * EIoTP version 1.0. There is a identically named function (without suffix)
 * that is used for protocol v2.0.
 */
void ebv_mpack_set_next_events_object_v10(struct ebv_mpack *mpack, struct ebv_mpack_requests_object *obj)
{
	cw_pack_map_size(mpack->pack, 2);
	ebv_mpack_add_str(mpack, "type");
	ebv_mpack_add_str(mpack, "status");  /* Quick and dirty: Fixed string */
	ebv_mpack_add_str(mpack, "data");
	return;
}

int ebv_mpack_strcmp(char *str, cwpack_blob *cwpstr)
{
	int ret;
	size_t len = strlen(str);
	if (cwpstr->length != len) {
		ret = -1;
	} else {
		ret = strncmp(str, cwpstr->start, len);
	}
	return ret;
}

void ebv_mpack_add_str(struct ebv_mpack *mpack, char *str)
{
	cw_pack_str(mpack->pack, str, strlen(str));
	return;
}

bool ebv_mpack_is_str(char *refstr, struct ebv_mpack *mpack)
{
	cwpack_blob *str;
	cw_unpack_next(mpack->unpack);
	if (mpack->unpack->item.type != CWP_ITEM_STR) {
		Throw(EBV_MPACK_REQUEST_PARSE_ERR);
	}
	str = &(mpack->unpack->item.as.str);
	if (ebv_mpack_strcmp(refstr, str) == 0) {
		return true;
	} else {
		return false;
	}
}
