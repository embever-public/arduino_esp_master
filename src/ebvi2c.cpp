/*
 * ebvi2c.c
 *
 *  Created on: 7 Mar 2018
 *      Author: pablo
 */

#include "CException.h"

#include "ebverr.h"
#include "ebvconf.h"
#include "ebvi2chal.h"
#include "ebvi2c.h"

/* I2C0 */
/* I2C2 */
static uint8_t _ebv_i2c2_tx_buf[EBV_CONF_I2C_MASTER_TX_BUF_SIZE];
static uint8_t _ebv_i2c2_rx_buf[EBV_CONF_I2C_MASTER_RX_BUF_SIZE];
uint8_t *ebv_i2c2_tx_buf = (uint8_t *) _ebv_i2c2_tx_buf;
uint8_t *ebv_i2c2_rx_buf = (uint8_t *) _ebv_i2c2_rx_buf;
static struct ebv_i2c _ebv_i2c2;
struct ebv_i2c *ebv_i2c2 = &_ebv_i2c2;

/* Slave call backs */
static struct ebv_i2c_hal_slave_xfer _ebv_i2c0_scb;
struct ebv_i2c_hal_slave_xfer *ebv_i2c0_scb = &_ebv_i2c0_scb;
static struct ebv_i2c_hal_slave_xfer _ebv_i2c1_scb;
struct ebv_i2c_hal_slave_xfer *ebv_i2c1_scb = &_ebv_i2c1_scb;
static struct ebv_i2c_hal_slave_xfer _ebv_i2c2_scb;
struct ebv_i2c_hal_slave_xfer *ebv_i2c2_scb = &_ebv_i2c2_scb;


/* Definition of extern functions */

void ebv_i2c_set_iface_role(uint8_t id, uint8_t addr)
{
    const uint8_t minaddr = 0x10;
    const uint8_t maxaddr = 0x6F;
    if (EBV_I2C_HAL_IFACE_NUM <= id) {
        Throw(EBV_I2C_INIT_ERR);
    }
    if (EBV_I2C_IFACE_ROLE_MASTER == addr) {
    	ebv_i2c_hal_setup_master((ebv_i2c_hal_iface_id_t) id);
    } else if ((minaddr <= addr) && (maxaddr >= addr)) {
    	ebv_i2c_hal_setup_slave((ebv_i2c_hal_iface_id_t) id, addr);
    } else {
    	Throw(EBV_I2C_INIT_ERR);
    }
    return;
}

void ebv_i2c_master_txrx_request(const struct ebv_i2c *i2c,
        const uint8_t slaveaddr, uint8_t *txbuf, uint16_t txlen, uint8_t *rxbuf,
        uint16_t rxlen)
{
    int i;
    if (txlen > i2c->txlen) {
        Throw(EBV_I2C_BUFSIZE_ERR);
    }
    if (rxlen > i2c->rxlen) {
        Throw(EBV_I2C_BUFSIZE_ERR);
    }
    for (i = 0; i < txlen; i++) {
        i2c->txbuf[i] = txbuf[i];
    }
    ebv_i2c_hal_master_txrx_request(i2c->id, slaveaddr, i2c->txbuf, txlen,
            i2c->rxbuf, rxlen);
    for (i = 0; i < rxlen; i++) {
        rxbuf[i] = i2c->rxbuf[i];
    }
    return;
}

void ebv_i2c_start_slave(const struct ebv_i2c *i2c,
		struct ebv_i2c_hal_slave_xfer *xfer)
{
	ebv_i2c_hal_start_slave(i2c->id, xfer);
}

void ebv_i2c_stop_slave(const struct ebv_i2c *i2c)
{
	ebv_i2c_hal_stop_slave(i2c->id);
}

void ebv_i2c_stop_master(const struct ebv_i2c *i2c)
{
	ebv_i2c_hal_stop_master(i2c->id);
}

