## Table of contents

* [1. Description](#1-description)
* [2. Setup](#2-setup)
    * [2.1 Hardware Setup](#21-hardware-setup)
    * [2.2 Library installation](#22-library-installation)
* [3. Usage](#3-usage)
* [Conclusion](#conclusion)
<br/>

## 1. Description

With help of Embever development board together with Arduino it is extremely convenient to create and 
transmit the block of data, called **Event**. *Event* consists of its name, name of the informative parameter and the value of this parameter. 
Generation of such an *Event*s is implemented within the current ``arduino_esp_master.h``
header, which is opened for your trial usage. It requires only the simple hookup configuration and installation of the library.
Additionally you would have to provide a reliable power supply to the Embever development board and you are good to go.

<br/>

## 2. Setup

*Embever Serial Protocol* (**ESP**) provides the communication between two devices: **ESP SLAVE** and **ESP MASTER**.<br/>

**ESP Master**
___
*ESP Master* is supposed to handle all of the internal processes, required by a certain application purposes: 
getting the sensors' data, manipulation over the connected devices (e.g. LED, Sound Speaakers…).<br/>
Let's call these devices *Application devices* (**APP DEV**).<br/> 

**ESP Slave**
___
Embever Development Board (**DEV BOARD**) at its turn plays the role of *ESP Slave*. According to the *ESP* after period of 
inactivity *ESP Slave* wakes up and initialises its interfaces. When *ESP Slave* is ready for commands' reception, 
it pulls down the **READY pin** and waits for commands from the *ESP Master*. Since the command is received *ESP Slave* 
takes some time to process it, and after the command is implemented it pulls down the **IRQ pin** to signalise that the response 
message can be requested.<br/>

> NOTE. *ESP Master* has two more pins to control the state of the *ESP Slave*: **WAKE** and **RESET**.
<br/>

In the current setup *Arduino* plays the role of the *ESP Master*, *Dev Board* - *ESP Slave* and *Bug Counter* - *App Dev*.

<a name="fig21">
</a>

<div align="center">
![Image](_readme_images/setup.png)
</div>

<div align="center">
  Figure 2.1 – Basic System Configuration
</div>
<br/>

#### 2.1 Hardware Setup

**Power Supply**
___

*Dev Board*: Direct Current Power Supply (+3V8, at least 500mA)<br/>

> NOTE. Connect the power supply only to the dedicated pins mentioned in [Figure 2.1.1](#fig211) as 
*Power Supply Input* to avoid loops.<br/> The *+1V8 Source Output* is provided by LDO on the
*Dev Board*. Use this output to power the low-voltage side of the level converter. 
DO NOT connect an external power supply to +1V8 pin.

*Arduino*: supply power as required by the official *Arduino* documentation

**Level Shifting**
___

Since there is a difference in Logic Voltage Levels between *ESP Master* and *ESP Slave* (+3V3 and +1V8 correspondingly), the usage
of additional Level Shifter is required.
For this purpose see the suggestion in [Figure 2.1.1](#fig211):<br/>
"Adafruit 4 Channel Level Shifter PCB - 4 BSS138 FETs with 10K pullups"

**Asynchronous signals' processing**
___

As it was mentioned above, *ESP* uses some signals to figure out the state of each other: slave and the master.<br/>
See the configuration of those pins in the file `arduino_esp_master.h` after accomplishing the actions in the Chapter [2.2 Library installation](#library-installation)

**HookUp guide**
___

The following Scheme can be used as the HookUp guide.<br/>
Please contact us for any questions regarding to the connection of *Dev Board*.

<a name="fig211">
</a>

<div align="center">
  ![Image](_readme_images/connection_scheme_arduino_uno.png)
</div>

<div align="center">
  Figure 2.1.1 – HookUp Scheme
</div>
<br/>

#### 2.2 Library installation

**Installation from .zip**
___

To install the libraries from the .zip file, please follow the instructions in the official Adruino manual: [Installing Additional Arduino Libraries](https://www.arduino.cc/en/guide/libraries). 
<br/>
You can find *arduino_esp_master_port.zip* archive in the root directory for this project.


**Unix based systems**
___

Another way to install the *arduino_esp_master_port* library is to manually copy all of the files into your *Arduino* folder.<br/>
For an easy installation of the required libraries, please run the script `lib_linker.sh`. It only makes soft links of the required files to the folder with *Arduino* libraries. 
Or you can do it manually (see the manual for *Windows* further).

**Windows (manual installation)**
___

To install the libraries you can simply create soft links for all of the files inside `inc/` and `src/` folders, 
and move them to your `Arduino/libraries/arduino_esp_master` folder

<br/>

## 3. Usage


***setup()***
___

To use the *arduino_esp_master* library, please `#include` it in the project
```cpp
#include "arduino_esp_master.h"
```

First of all for the initialisation of *Arduino* as the *ESP Master* user must call two functions from `arduino_esp_master.h`:

```cpp
arduino_esp_pins_init();
arduino_esp_master_init();
```
> NOTE. See the pin configuration in the top of `arduino_esp_master.h`

***Application Scope***
___

Basically the scope of the current application seems to be as following:<br/>
1. Manipulation over the sensor (*Bug Counter*), which is application specific;
2. Collection of informative parameter's value (e.g. amount of bugs);
3. Main condition (when the data should be sent to the cloud: e.g. once per hour, once per 100 bugs caught…);
4. Generation of the *Event* to send (creation of the *Event*'s and parameter's name together with the current parameter's value; 
building of the message pack, which is handled by the `arduino_esp_master.h`);
5. Transmission of the generated *Event* via *ESP* to the *ESP Slave* and waiting for the response (handled by the `arduino_esp_master.h` as well)…

***Basic Implementation***
___

Let's go through above steps more specific. As a reference please, use `examples/arduino_esp_master_event_example/arduino_esp_master_event_example.ino` file,
which dmonstrates the way how the *Event* could be created and sent via *ESP*.

- ***Steps 1-3*** are entirely up to user. The main thing is to collect the informative parameter's value, which will be sent further within
the *Event*'s data.<br/>
The application specific code can be inserted at the very beginning of the main_loop:

*arduino_esp_master_event_example.ino*
```cpp
void loop() {
  /* 
   *  Insert the application specific code here
   */

// …
```

One important thing here could be the reception of debug messages via UART Serial Monitor. `arduino_esp_master.h` 
provides some debug messages on the most significant steps of creation and transmission of *Event*'s data via *ESP*.<br/>
User can enable or disable those by commenting/uncommenting of the definition *ARDUINO_ESP_MASTER_SERIAL_DEBUG*,
which is on the top of the header:

*arduino_esp_master.h*
```cpp
/* ATTENTION: Comment to disable reception of 
 * messages via the UART Serial Monitor 
 */
// #define ARDUINO_ESP_MASTER_SERIAL_DEBUG

// …
```

Be sure to enable the Serial Monitor beforehand if this parameter is active.

- ***Step 4***. When the data is collected user has to build an *Event* 
with predefined name of *Event* itself and parameter's name.

*arduino_esp_master_event_example.ino*
```cpp
char event_name[] = "test";
char param_name[] = "bugs";
uint64_t value = 1000;
event_built = arduino_esp_master_build_event(event_name, sizeof(event_name), 
                                param_name, sizeof(param_name), value);

// …
```

- ***Step 5***. When all the steps above are implemented, *ESP Master* should check if the *ESP Slave* is ready 
for commands' reception. The `if` condition below will check the *ESP Slave* is ready. If the 
*ESP Slave* is not ready, the whole loop will be aborted and begun again in predefined time delay:
*ARDUINO_ESP_MASTER_SLAVE_READY_TIMEOUT*. (This is the PoC implementation, to have more flexibility,
change this step at your purposes)

*arduino_esp_master_event_example.ino*
```cpp
if (!arduino_esp_master_check_wait_slave_ready()) {
    return;
  }

// …
```

> NOTE. To be sure that *ESP Slave* is awake after the *Deep Power Down* mode, please pull down
the `ARDUINO_ESP_MASTER_WAKE_SLAVE_PIN` pin shortly for 1 millisecond. If the *ESP Slave* still does not respond,
do the same procedure once with `ARDUINO_ESP_MASTER_RESET_SLAVE_PIN`. These methods are combined in the
function `get_esp_slave_ready()`, please see the `arduino_esp_master.h` for the description:

```cpp
static void wake_esp_slave(void) {
  digitalWrite(ARDUINO_ESP_MASTER_WAKE_SLAVE_PIN, LOW);
  delay(1);
  digitalWrite(ARDUINO_ESP_MASTER_WAKE_SLAVE_PIN, HIGH);
}

static void reset_esp_slave(void) {
  digitalWrite(ARDUINO_ESP_MASTER_RESET_SLAVE_PIN, LOW);
  delay(1);
  digitalWrite(ARDUINO_ESP_MASTER_RESET_SLAVE_PIN, HIGH);
}

bool void get_esp_slave_ready(void) {
  if (!arduino_esp_master_check_wait_slave_ready()) {
    wake_esp_slave();     // Try to wake ESP Slave if it doesn't respond
    delay(ARDUINO_ESP_MASTER_SLAVE_READY_TIMEOUT);
    if (!arduino_esp_master_check_wait_slave_ready()) {
      reset_esp_slave();  // Try to reset ESP Slave if it doesn't respond
      delay(2*ARDUINO_ESP_MASTER_SLAVE_READY_TIMEOUT);
      if (!arduino_esp_master_check_wait_slave_ready()) {
        return false;
      } 
    } 
  }
  return true;
}

//…
```

- ***Step 5: Event transmission***. When the *ESP Slave* is ready for the commands' reception we can finally send 
an *Event* to it. 

> NOTE. Be sure to check if the *Event* was built successfully (example is below).

```cpp
if (event_built) {
  event_sent = arduino_esp_master_send_event();
  if (event_sent) {
    Serial.print("Event is SET\n");
  } else {
    Serial.print("Event is NOT SET\n");
  }
  delay(ARDUINO_LOOP_CYCLE_DELAY);
  return;
} else {
    Serial.print("Event build is FAILED\n");
    delay(ARDUINO_LOOP_CYCLE_DELAY);
    return;
}

//…

```

<br/>

## Conclusion

Completing all of the above steps and requirements is enough to generate an *Event* and send it to the *Cloud* via *ESP*. <br/>
If you run into the difficulties in understanding this documentation, or have questions, we are looking forward to land you a helping hand.
