/**
* @brief Implementation of Embever Serial Protocol
 */

#ifndef INC_EBVESP_H_
#define INC_EBVESP_H_

#include <stdint.h>
#include <stdbool.h>

enum {
    EBVESP_I2C_SLAVE_ADDR = 0x35,
    EBVESP_I2C_MASTER_ADDR = 0x00,
};

typedef enum {
    EBVESP_QOS_0,
    EBVESP_QOS_1,
    EBVESP_QOS_2,
    EBVESP_QOS_3
} ebv_qos_levels;

enum {
    EBVESP_I2C_CMD_NO_COMMAND = 0x00,
    EBVESP_I2C_CMD_READ_DELAYED_RESP = 0xA1,
    EBVESP_I2C_CMD_GET_ACTIONS = 0xA2,
    EBVESP_I2C_CMD_PUT_RESULTS = 0xA3,
    EBVESP_I2C_CMD_PUT_EVENTS = 0xA4,
    EBVESP_I2C_CMD_PERFORM_ACTIONS = 0xA6,
    EBVESP_I2C_CMD_READ_LOCAL_FILE = 0xA7,
    EBVESP_I2C_SOP_SOA_ID = 0x56,
    EBVESP_I2C_SOP_SOR_ID = 0x55
};

enum {
    EBVESP_I2C_OVERHEAD_HEADER = 4,
    EBVESP_I2C_OVERHEAD_CRC = 4,
    EBVESP_I2C_LEN_ON_ERROR = 4,
    EBVESP_I2C_OVERHEAD_CSUM_FLAGS = 5,
    EBVESP_I2C_FIELD_COMMAND_ERR = 155,
    EBVESP_I2C_FIELD_SOP_ERR = 122,
    EBVESP_I2C_FIELD_ACK_ERR = 133,
    EBVESP_I2C_FLAGS_FIELD_QOS_ERR,
    EBVESP_I2C_RESPONSE_PKT_CONTAINS_ERR_CODE,
    EBVESP_I2C_RESPONSE_LEN_ERR
};

enum {
    ESP_ERR_INVALID_CMD_ID = 0x0101,
    ESP_ERR_INVALID_CMD_DATA = 0x0102,
    ESP_ERR_INVALID_RESP_DATA,
    ESP_ERR_INVALID_RESP_CMD_ID
};



/*****************************   M A S T E R   ********************************/

/**
 * @brief Setup the microcontroller as ESP I2C Master
 * @param irqpin: the pin number of the ESP IRQ line
 * @param readypin: the pin number of the ESP READY line
 *
 * Note that because ESP master and ESP slave use the same I2C interface,
 * only one role (ESP master or ESP slave) is active at any time.
 */
void ebv_esp_init_master(uint8_t irqpin, uint8_t readypin);

/**
 * @brief Check if EBV-uC (slave) ESP interface is ready
 * @return true if the ready pin is asserted (pulled low), false otherwise.
 */
bool ebv_esp_is_interface_ready(void);

/**
 * @brief Checks if EBV-uC (slave) ready to receive READ_DELAYED_RESP CMD
 * @param timeout: time in milliseconds to wait for the IRQ line to be asserted
 *
 * This function throw an error, if the waiting time for ESP_IRQ has expired.
 */
void ebv_esp_is_ready_for_rdr_cmd(uint32_t timeout);

/**
 * @brief App-uC (master) sends PUT_EVENTS CMD to Embever-uC (slave) via I2C
 * @param data: pointer to the data to be transmitted to the cloud.
 * @param data_length: the number of bytes to be transmitted.
 * @param disable_crc: 1: ignore CRC field (checksum=0),
 *                     0: do CRC check.
 * @param qos: Quality of services levels:
 *              0: do not confirm this command
 *              1: send a confirmation packet after execution of this command
 *              2: Reserved
 *              3: Reserved
 *
 * This function send a PUT_EVENTS command to the slave (Embever-uC) and further
 * to the cloud with a specified data. And it gets acknowledge packet and checks
 * it for errors.
 */
void ebv_esp_cmd_put_events(uint8_t *data, uint16_t data_len,
        uint8_t disable_crc, ebv_qos_levels qos);

/**
 * @brief App-uC (master) read a file stored by the Embever-uC (slave) via I2C
 * @param data: pointer to the getFile data object in messagePack format.
 * @param data_len: the combined length of data and checksum (the number of bytes)
 *              to be transmitted.
 * @param disable_crc: 1: ignore CRC field (checksum=0),
 *                     0: do CRC check.
 * @param qos: Quality of services levels:
 *              0: do not confirm this command
 *              1: send a confirmation packet after execution of this command
 *              2: Reserved
 *              3: Reserved
 *
 * This function sends a READ_LOCAL_FILE command to the slave (Embever-uC) and
 * rx an ack packet and checks it for errors.
 */
void ebv_esp_cmd_read_local_file(uint8_t *data, uint16_t data_len,
        uint8_t disable_crc, ebv_qos_levels qos);

/**
 * @brief AM (master) request the EM (slave) to perform certain actions on the AM
 * @param data: pointer to the actions data object in messagePack format,
 *              contains a list with the received action_list’s whose type is
 *              any of the action types supported by this command
 * @param data_len: the combined length of data and checksum (the number of bytes)
 *              to be transmitted.
 * @param disable_crc: 1: ignore CRC field (checksum=0),
 *                     0: do CRC check.
 * @param qos: Quality of services levels:
 *              0: do not confirm this command
 *              1: send a confirmation packet after execution of this command
 *              2: Reserved
 *              3: Reserved
 *
 * This function sends a READ_LOCAL_FILE command to the slave (Embever-uC) and
 * rx an ack packet and checks it for errors.
 */
void ebv_esp_cmd_perform_actions(uint8_t *data, uint16_t data_len,
        uint8_t disable_crc, ebv_qos_levels qos);

/**
 * @brief App-uC (master) sends READ_DELAYED_RESP CMD to EBV-uC (slave) via I2C
 * @param resp_pkt_data: Pointer to the payload of the response packet
 * @param resp_pkt_data_len: Pointer to the length of the payload
 * @return true if there was a delayed response packet pending, false otherwise
 *
 * This function send a READ_DELAYED_RESP command to the slave (Embever-uC) to
 * get the response of a command with delayed response. It checks the length of
 * response and throw an error if the length is bigger than rx buffer length.
 *
 * This command can be issued by the master even though the IRQ line has not
 * been asserted by the slave (i.e. there is no delayed response yet).
 * In such cases, this function returns false, and *resp_pkt_data=NULL and
 * data_len=0.
 *
 * If there is a delayed response packet (i.e. after IRQ was asserted by the
 * slave), this function returns true. In this case,
 * if the delayed response packet has no data (i.e. data_len=0), then
 * *resp_pkt_data=NULL. Otherwise, if data_len>0, *resp_pkt_data points to the
 * data in the delayed response packet.
 *
 * For using with a response data bigger than rx buffer length we must agree on
 * where will be this data stored.
 */
bool ebv_esp_cmd_read_delayed_resp(uint8_t **resp_pkt_data, uint16_t *resp_pkt_data_len);

/**
 * @brief Sends cmd to Embever-uC (slave) and gets the length of delayed response
 * @param cmd: Read Delayed Response 0xA1.
 * @return Length of response delayed response
 */
uint16_t ebv_esp_tx_cmd_rx_response_packet(uint8_t cmd);

/**
 * @brief Evaluates the length and throw an error if rx buffer is too small
 * @param response_length: Length of response of a command with delayed response
 *
 * It throw an error if the response_length is bigger than rx buffer length.
 */
void ebv_esp_eval_response_data_len(uint16_t response_length);

/**
 * @brief Builds a flags byte using input parameters
 * @param ignore_crc: 1: ignore CRC field (checksum=0),
 *                    0: do CRC check.
 * @param qos: Quality of services levels:
 *              0: do not confirm this command
 *              1: send a confirmation packet after execution of this command
 *              2: Reserved
 *              3: Reserved
 * @return One byte (flags field) of coded information.
 */
uint8_t ebv_esp_build_flags_field(bool ignore_crc, ebv_qos_levels qos);

/**
 * @brief App-uC (master) sends GET_ACTIONS CMD to Embever-uC (slave) via I2C
 *
 * This function send a GET_ACTIONS command to the slave (Embever-uC) and further
 * to the cloud. It gets acknowledge packet and checks it for errors.
 *
 * To get a response packet, use the ebv_esp_cmd_read_delayed_resp() function
 * after ESP_IRQ goes Low.
 */
void ebv_esp_cmd_get_actions(void);

/**
 * @brief Sends command to Embever-uC (slave) via I2C and checks acknowledge packet
 * @param cmd_packet: pointer to the command packet.
 * @param cmd_packet_len: the length of the command packet to be transmitted.
 */
void ebv_esp_tx_cmd_rx_ack_and_check(uint8_t *cmd_packet, uint16_t cmd_packet_len);

/**
 * @brief App-uC (master) sends PUT_RESULTS CMD to Embever-uC (slave) via I2C
 * @param data: pointer to the data to be transmitted to the cloud.
 * @param data_len: the number of bytes to be transmitted.
 * @param disable_crc: 1: ignore CRC field (checksum=0),
 *                     0: do CRC check.
 * @param qos: Quality of services levels:
 *              0: do not confirm this command
 *              1: send a confirmation packet after execution of this command
 *              2: Reserved
 *              3: Reserved
 *
 * This function send a PUT_RESULTS command to the slave (Embever-uC) and further
 * to the cloud with a specified data. And it gets acknowledge packet and checks
 * it for errors.
 */
void ebv_esp_cmd_put_results(uint8_t *data, uint16_t data_len,
        uint8_t disable_crc, ebv_qos_levels qos);

/**
 * @brief App-uC (master) sends a specified command packet with data to the slave
 *
 * This function sends a specified command packet with data to the Embever-uC
 * (slave) via I2C. It gets acknowledge packet and checks it for errors.
 *
 * @param cmd: command to be transmitted.
 * @param data: pointer to the data to be transmitted to the cloud.
 * @param data_len: the number of bytes to be transmitted.
 * @param disable_crc: 1: ignore CRC field (checksum=0),
 *                     0: do CRC check.
 * @param qos: Quality of services levels:
 *              0: do not confirm this command
 *              1: send a confirmation packet after execution of this command
 *              2: Reserved
 *              3: Reserved
 */
void ebv_esp_send_command_packet_with_data(uint8_t cmd, uint8_t *data,
        uint16_t data_len, uint8_t disable_crc, ebv_qos_levels qos);

/**
 * @brief Evaluates the response data from the ESP slave
 *
 * The ESP master checks the response data from the ESP slave. An error will occur
 * if the number of bytes actually received is less than or equal to the overhead
 * of the response packet, or if the calculated crc does not match the crc
 * received in the response packet (in case it is activated by flags).
 *
 *  @param rx_bytes: Number of bytes in the response packet received by the master
 */
void ebv_esp_eval_response_packet_with_data(uint16_t rx_bytes);


/******************************   S L A V E   *********************************/

/**
 * @brief Setup Embever microcontroller as ESP I2C slave
 * @param irqpin: the pin number of the ESP IRQ line
 * @param readypin: the pin number of the ESP READY line
 *
 * To start this interface use ebv_i2c_start_slave().
 * Note that because ESP master and ESP slave use the same I2C interface,
 * only one role (ESP master or ESP slave) is active at any time.
 */
void ebv_esp_init_slave(uint8_t irqpin, uint8_t readypin);

/**
 * @brief start the ESP I2C slave interface to run in the background.
 *
 */
void ebv_esp_start_slave(void);
void ebv_esp_stop_slave(void);

/**
 * @brief Send acknowledgment to the ESP (I2C) master
 * @param cmd_packet: received command packet from the ESP (I2C) master.
 */
void ebv_esp_ack_packet(uint8_t *cmd_packet);

/**
 * @brief Build a header of a response packet and put it to ebv_esp_header_buf[]
 *
 * It'll be send by ebv_esp_slave_send() callback, and it has following fields:
 *      sop     -- Start of response packet identifier
 *      command -- Process command identifier
 *      length  -- The overall amount of data still to be sent
 *It will be send by ebv_esp_slave_send() callback.
 * @param cmd: Command field identifier
 * @param resp_len: The content of the length field in the response packet
 */
void ebv_esp_build_response_packet_header(uint8_t cmd, uint16_t resp_len);

/**
 * @brief Copy a header of a response packet to the specified buffer
 *
 * Copies the header of a response packet, previously created by the function
 * ebv_esp_build_response_packet_header(), to the specified buffer.
 *
 * @param buf: Target buffer for the previously created header
 */
void ebv_esp_copy_response_packet_header_to_buf(uint8_t *target_buf);

/**
 * @brief Write a actions data to the data pending buffer and set irqpin low
 *
 * The master receives this data by sending read delayed response command to the
 * slave. In the ebv_esp_slave_done() callback a function
 * ebv_esp_write_response_packet_to_tx_buf() will be called. It copies the data
 * pending buffer to the tx buffer.
 *
 * @param *data: pointer to the response data
 * @param data_len: the length of the response data
 * @param ignore_crc: 1: ignore CRC field (checksum=0),
 *                    0: do CRC check.
 * @param qos: Quality of services levels:
 *              0: do not confirm this command
 *              1: send a confirmation packet after execution of this command
 *              2: Reserved
 *              3: Reserved
 * @return The data length in the data pending buffer
 */
uint16_t ebv_esp_copy_response_w_actions_data_to_data_pending_buf(uint8_t *data,
        uint16_t data_len, bool ignore_crc, ebv_qos_levels qos);

/**
 * @brief Write a response with no data to the data pending buf, set irqpin low
 *
 * The master receives this data by sending read delayed response command to the
 * slave. In the ebv_esp_slave_done() callback a function
 * ebv_esp_write_response_packet_to_tx_buf() will be called. It copies the data
 * pending buffer to the tx buffer.
 *
 * @return The data length in the data pending buffer
 */
uint16_t ebv_esp_copy_response_w_no_data_to_data_pending_buf(void);

/**
 * @brief Write a response with error code to the data pending buf, set irqpin low
 * @param cmd: command id
 * @param err: a two byte error code
 * @return The data length in the data pending buffer
 *
 * The master receives this data by sending read delayed response command to the
 * slave. In the ebv_esp_slave_done() callback a function
 * ebv_esp_write_response_packet_to_tx_buf() will be called. It copies the data
 * pending buffer to the tx buffer.
 */
uint16_t ebv_esp_copy_response_packet_error_to_data_pending_buf(
		uint8_t cmd, uint16_t err);

/**
 * @brief Prepare a whole response packet and copy it to the txbuf
 * @param response_len: Length of (delayed) response. This length defines how
 *          many bytes from data pending buffer to sent.
 * @return true: It signals that the data is being sent.
 */
bool ebv_esp_write_response_packet_to_tx_buf(uint16_t response_len);

/**
 * @brief Prepare a whole response packet with an error and copy it to the txbuf
 * @param cmd: Received command
 * @param err: Error code
 */
void ebv_esp_write_response_packet_error_to_tx_buf(uint8_t cmd, uint16_t err);

/**
 * @brief Evaluates rx length (data + crc) and checksum of command packet
 *
 * @param rx_bytes: Number of bytes received by the slave_recv() callback
 * @return 0 if the rxbuf packet length and crc are valid (cmd_pkt_ok),
 *  ESP_ERR_INVALID_CMD_DATA otherwise.
 *
 *
 * Returns a two-byte error code if the ESP rxbuf's
 * combined length of data and checksum does not match the expected length or if
 * the checksum fails (in case it is activated by flags).
 * Return 0 otherwise (the bytes in the rxbuf have valid information)
 */
uint16_t ebv_esp_eval_command_packet(uint16_t rx_bytes);

#endif /* INC_EBVESP_H_ */
