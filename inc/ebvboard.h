/**
 *@brief board interface for EBV_R4_L5_VOSESI_V0
 *
 */

#ifndef INC_EBVBOARD_H_
#define INC_EBVBOARD_H_

#include <stdint.h>


bool ebv_digital_read(uint8_t pin);
void ebv_digital_write(uint8_t pin, bool state);
bool ebv_board_waiting_for_esp_irq_pin_goes_low(uint8_t pin, uint32_t timeout);
void ebv_delay(uint32_t t_ms);

#endif /* INC_EBVBOARD_H_ */
