/**
 *@brief Basic I2C interface functions
 *
 */


#ifndef INC_EBVI2C_H_
#define INC_EBVI2C_H_

#include <string.h>
#include <stdint.h>

#include "ebvi2chal.h"

enum {
	EBV_I2C_IFACE_ROLE_MASTER = 0,
};

/**
 *@struct ebv_i2c
 *@brief contains main I2C transmission parameters
 */
struct ebv_i2c {
	uint8_t id;  /**< The interface identifier */
	uint8_t *txbuf;  /**< Pointer to array where to copy the bytes to be sent */
	size_t txlen;  /**< Maximum length of the txbuf array */
	uint8_t *rxbuf;  /**< Pointer to array where to copy the bytes received */
	size_t rxlen;  /**< Maximum length of the rxbuf array */
};

/*@brief set the role as a I2C interface as master or slave
 *@param id: the interface number (starting from 0)
 *@param addr: use 0 to define the interface as master,
 * 		otherwise the value given is the slave address (if valid)
 *
 * The number of interfaces supported depends on the i2chal.
 * An exception is thrown if an invalid id is given,
 * or if the slave address is not valid.
 *
 * Valid address values go from 0x10 to 0x6F. It is recommended to use values
 * where the bits alternate (0x3F and 0x40 are bad examples). Nice values
 * are anything with a 5 or and A in it like 0x2A, 0x15. Other good examples
 * are 0x2C, 0x69.
 */
void ebv_i2c_set_iface_role(uint8_t id, uint8_t addr);

/**
 *@brief start the I2C slave, and run callbacks when a master request arrives
 *@param id: the I2C hardware interface id
 *@param xfer: pointer to structure with the pointers to callback functions
 *
 * The callback functions in the xfer are application specific,
 * and need to be implemented by the user. Similarly, xfer must point to a
 * structure containing the function pointers (to be done by the user).
 */
void ebv_i2c_start_slave(const struct ebv_i2c *i2c,
		struct ebv_i2c_hal_slave_xfer *xfer);

/**
 *@brief send a request to a slave and waits for an answer (if any)
 *@param i2c: a structure with the I2C transmission parameters
 *@param slaveaddr: a valid 7-bit slave address
 *@param txbuf: pointer to an array from which to read the data to be transmitted
 *@param txlen: the number of bytes to be transmitted (can be 0)
 *@param rxbuf: pointer to an array in which to store the received data
 *@param rxlen: the number of bytes to be received (can be 0)
 *
 * Under normal circumstances, before sending data, txlen bytes from txbuf are
 * copied into i2c->txbuf. After receiving rxlen bytes, they are copied from
 * i2c->rxbuf into rxbuf.
 *
 * The i2c must point to a structure properly configured with
 * large enough arrays. The If txlen (rxlen) is larger than i2c->txlen (i2c->rxlen)
 * an exception is thrown.
 *
 * If there is no answer from the slave (e.g. slaveaddr does not match any slave
 * in the bus), an exception is thrwon (timeout).
 */
void ebv_i2c_master_txrx_request(const struct ebv_i2c *i2c, const uint8_t slaveaddr,
		uint8_t *txbuf, uint16_t txlen, uint8_t *rxbuf, uint16_t rxlen);

/**
 *@brief stop all slave activity and deinitialize interfaces
 *@param i2c: a structure with the I2C transmission parameters
 */
void ebv_i2c_stop_slave(const struct ebv_i2c *i2c);

/**
 *@brief stop all master activity and deinitialize interfaces
 *@param i2c: a structure with the I2C transmission parameters
 */
void ebv_i2c_stop_master(const struct ebv_i2c *i2c);

/*@brief test function to initialize i2c interfaces
 *
 * FIXME: this function must be defined in the board file, as it is
 * application dependent
 */
void ebv_i2c_init(void);

#endif /* INC_EBVI2C_H_ */
