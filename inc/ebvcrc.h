/**
 * @brief Functions to compute cyclic redundancy check (CRC)
 */

#ifndef INC_EBVCRC_H_
#define INC_EBVCRC_H_

#include <stdint.h>

/**
 * @brief Compute a standard CRC32 checksum from an 8-bit data block
 * @param data: Pointer to the block of 8-bit data
 * @param numbytes: The number of 8-bit entries pointed to by data
 * @return CRC32 sum value
 *
 * The CRC32 is computed using 32-bit words, with the last word padded with 1s.
 * That is, if numbytes is not a multiple of 4, (i.e. numbytes % 4 != 0),
 * the last 32-bit word is formed using the last (numbytes % 4) in data, and
 * the remaining bytes in the 32-bit word are filled with 0xff (padding).
 */
uint32_t ebv_crc_compute_crc32(const uint8_t *data, uint32_t numbytes);



#endif /* INC_EBVCRC_H_ */
