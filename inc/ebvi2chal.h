/**
 *@brief I2C hardware abstraction layer interface
 *
 */

#ifndef EBVI2CHAL_H_
#define EBVI2CHAL_H_

#include <stddef.h>
#include <stdint.h>

typedef enum {
    EBV_I2C_HAL_IFACE_I2C0,     /* Identifier for I2C Interface I2C0 */
    EBV_I2C_HAL_IFACE_I2C1,     /* Identifier for I2C interface I2C1 */
    EBV_I2C_HAL_IFACE_I2C2,     /* Identifier for I2C interface I2C2 */
#ifdef EBV_DEBUGPOSIX
	/* We use the same low level I2C HAL interface for SPI HAL in POSIX */
    EBV_I2C_HAL_IFACE_SPI0,     /* Identifier for I2C interface SPI0 */
    EBV_I2C_HAL_IFACE_SPI1,     /* Identifier for I2C interface SPI1 */
#endif
    EBV_I2C_HAL_IFACE_NUM,      /* Number of available interfaces */
} ebv_i2c_hal_iface_id_t;

typedef enum {
    EBV_I2C_HAL_ROLE_NONE,      /* Interface left without I2C configured */
    EBV_I2C_HAL_ROLE_MASTER,    /* Interface configured as I2C master */
    EBV_I2C_HAL_ROLE_SLAVE,     /* Interface configured as I2C slave */
} ebv_i2c_hal_iface_role_t;

/**
 * @brief I2C slave service start callback
 * @param addr: the 7-bit address received from the master
 *
 * This callback is called from the I2C slave handler when a matching I2C slave
 * address is received and needs servicing. It's used to indicate the start of
 * a slave transfer that will happen on the slave bus.
 */
typedef void (*ebv_i2c_hal_slave_xfer_start) (uint8_t addr);

/**
 * @brief I2C slave send data callback
 * @param data: a pointer to the byte to send to the master
 * @return non-0 to finish the transfer, 0 to continue sending data
 *
 * This callback is called from the I2C slave handler when an I2C slave address
 * needs data to send (transfer type: write).
 * Return non-0 to NACK the master and terminate the transfer, or return
 * a 0 value with the value to send in *data.
 */
typedef uint8_t(*ebv_i2c_hal_slave_xfer_send) (uint8_t *data);

/**
 * @brief I2C slave receive data callback
 * @param data: the byte received from the master
 * @return non-0 to finish the transfer, 0 to continue receiving data
 *
 * This callback is called from the I2C slave handler when an I2C slave address
 * has receive data (transfer type: read).
 * Return non-0 to NACK the master and terminate the transfer, or return
 * a 0 value to continue the transfer.
 */
typedef uint8_t(*ebv_i2c_hal_slave_xfer_recv) (uint8_t data);

/** @brief I2C slave service done callback
 *
 * This callback is called from the I2C slave handler when an I2C slave transfer is
 * completed. It's used to indicate the end of a slave transfer (read or write).
 */
typedef void (*ebv_i2c_hal_slave_xfer_done) (void);

/**
 *@struct ebv_i2c_hal_slave_xfer
 *@brief contains pointer to functions to be called during I2C slave activity
 *
 * Slave transfer are performed using 4 callbacks. These callbacks handle most
 * I2C slave transfer cases. When the slave is started the master initiate some
 * activity one of these callbacks is called.
 * The callbacks can be used for unsized transfers from the master.
 *
 * When an address is received, the start() callback is called with the
 * received address. Only addresses enabled for the slave will be
 * handled.
 *
 * If the master is going to perform a read operation, the send() callback
 * is called. Place the data byte to send in *data and return a 0 value to the
 * caller, or return non-0 to NACK the master (slave has finished).
 *
 * If the master performs a write operation, the recv() callback is called
 * with the received data. Return a 0 value to the caller, or return
 * non-0 to NACK the master (slave has finished).
 *
 * Once the transfer completes, the done() callback will be called.
 */
struct ebv_i2c_hal_slave_xfer {
    ebv_i2c_hal_slave_xfer_start start; /*!< Called when a matching I2C address is received */
    ebv_i2c_hal_slave_xfer_send send;   /*!< Called when a byte is needed to send to master */
    ebv_i2c_hal_slave_xfer_recv recv;   /*!< Called when a byte is received from master */
    ebv_i2c_hal_slave_xfer_done done;   /*!< Called when a slave transfer is complete */
};

/**
 *@brief configure the I2C hardware interface as a slave
 *@param id: the I2C hardware interface id
 *@param txbuf: pointer to an array from which to read the data to be transmitted
 *@param sizeof_txbuf: the maximum size of the txbuf array
 *@param rxbuf: pointer to an array in which to store the received data
 *@param sizeof_rxbuf: the maximum size of the rxbuf array
 *
 * Call this function before calling any other i2c function on an interface
 * The pin configuration of the interface is not done inside this function.
 */
void ebv_i2c_hal_setup_slave(ebv_i2c_hal_iface_id_t id, uint8_t slaveaddr);

/**
 *@brief start the I2C slave, and run callbacks when a master request arrives
 *@param id: the I2C hardware interface id
 *@param start: function to call when transmission starts
 *@param send: function to call before sending the next byte of data
 *@param recv: function to call when a new byte of data is received
 *@param done: function to call when the transmission is finished
 *
 * The callback functions (start, send, recv, done) are application specific,
 * and need to be implemented by the user.
 *
 * Call this function only after calling ebv_i2c_hal_setup_slave
 * using the same id. Otherwise an error is thrown.
 */
void ebv_i2c_hal_start_slave(ebv_i2c_hal_iface_id_t id,
                             struct ebv_i2c_hal_slave_xfer *xfer);

/**
 *@brief stop the I2C slave, no more requests from master will be processed
 *@param id: the I2C hardware interface id
 *
 * Call this function after calling ebv_i2c_hal_start_slave to stop
 * all slave activity
 */
void ebv_i2c_hal_stop_slave(ebv_i2c_hal_iface_id_t id);

/**
 *@brief configure the I2C hardware interface as a master
 *@param id: the I2C hardware interface id
 *
 * Call this function before calling any other i2c function on an interface
 * The pin configuration of the interface is not done inside this function.
 */
void ebv_i2c_hal_setup_master(ebv_i2c_hal_iface_id_t id);

/**
 *@brief stop the I2C master, no more requests to slave will be sent
 *@param id: the I2C hardware interface id
 *
 * Call this function after calling ebv_i2c_hal_setup_master to
 * properly deinitialize the master interface
 */
void ebv_i2c_hal_stop_master(const ebv_i2c_hal_iface_id_t id);

/**
 *@brief send a request to a slave and waits for an answer (if any)
 *@param id: the I2C hardware interface id
 *@param slaveaddr: a valid 7-bit slave address
 *@param txbuf: pointer to an array from which to read the data to be transmitted
 *@param txlen: the number of bytes to be transmitted (can be 0)
 *@param rxbuf: pointer to an array in which to store the received data
 *@param rxlen: the number of bytes to be received (can be 0)
 *
 * Call this function only after calling ebv_i2c_hal_setup_master
 * using the same id. Otherwise an error is thrown.
 */
void ebv_i2c_hal_master_txrx_request(const ebv_i2c_hal_iface_id_t id,
                                     const uint8_t slaveaddr,
                                     uint8_t *txbuf, uint16_t txlen,
                                     uint8_t *rxbuf, uint16_t rxlen);

#endif /* EBVI2CHAL_H_ */
