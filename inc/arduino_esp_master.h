/** arduino_esp_master.h
 *  
 *  Contains functions to put an Event
 *  
 *  Created on: 31.01.2020
 *  Author: Yuriy Golotsevich
 */

#ifndef _ARDUINO_ESP_MASTER_H_
#define _ARDUINO_ESP_MASTER_H_

#include "CException.h"
#include "Arduino.h"
#include "Wire.h"
#include "ebvesp.h"
#include "ebvmpack.h"
#include "extcwpack.h"
#include "extcwpack_defines.h"

/* ATTENTION: Comment to disable reception of 
 * messages via the UART Serial Monitor 
 */
// #define ARDUINO_ESP_MASTER_SERIAL_DEBUG

/* ATTENTION: CRC disabled by default
 * No CRC support is available for this
 * application
 */
#define ARDUINO_ESP_MASTER_DISABLE_CRC 1

/* ESP related structures */
enum {
    /* Pin configuration… 
     * All the pins are "active LOW"
     */
    ARDUINO_ESP_MASTER_RESET_SLAVE_PIN = 4,
    ARDUINO_ESP_MASTER_WAKE_SLAVE_PIN = 2,
    ARDUINO_ESP_MASTER_IRQ_PIN = 7,
    ARDUINO_ESP_MASTER_READY_PIN = 8,
    /* …Pin configuration */

    ARDUINO_WIRE_I2C_BUF_MAX_SIZE = 32,
#ifdef ARDUINO_ESP_MASTER_DISABLE_CRC
    ARDUINO_ESP_MASTER_I2C_MAX_DATA_SIZE = ARDUINO_WIRE_I2C_BUF_MAX_SIZE-
                        EBVESP_I2C_OVERHEAD_HEADER-0,
#else
    ARDUINO_ESP_MASTER_I2C_MAX_DATA_SIZE = ARDUINO_WIRE_I2C_BUF_MAX_SIZE-
                        EBVESP_I2C_OVERHEAD_HEADER-EBVESP_I2C_OVERHEAD_CRC, 
#endif
    ARDUINO_ESP_MASTER_I2C_MAX_MPACK_SIZE = ARDUINO_ESP_MASTER_I2C_MAX_DATA_SIZE,
    ARDUINO_ESP_MASTER_DR_TIMEOUT = 120000,         // Read Delayed Response timeout, default: 120000
    ARDUINO_ESP_MASTER_SLAVE_READY_TIMEOUT = 3000,  // Timeout for ESP Slave to rise the READY Pin, default: 3000
};




/* Arduino related functions */

/*@brief Initialisation of ESP related pins in required state (HIGH)
 */
void arduino_esp_pins_init(void);

/*@brief Read ESP related pins and writes their state to the Serial Monitor
 * 
 * ATTENTION: This function intended to work only with enabled Serial Monitor,
 * get sure that ARDUINO_ESP_MASTER_SERIAL_DEBUG is defined above in the
 * current header
 */
void check_pins(void);

/*@brief Wakes/Resets the ESP Slave to make it ready for commands' processing
 *
 *@return true if ready; false - if not
 */
bool get_esp_slave_ready(void);

/*@brief Print one character as hex to the Serial Monitor 
 *
 * ATTENTION: This function intended to work only with enabled Serial Monitor,
 * get sure that ARDUINO_ESP_MASTER_SERIAL_DEBUG is defined above in the
 * current header
 * 
 *@param one_byte: byte to be written
 */
void printHex(uint8_t one_byte);

/*@brief Print array of characters as hex to the Serial Monitor 
 *
 * ATTENTION: This function intended to work only with enabled Serial Monitor,
 * get sure that ARDUINO_ESP_MASTER_SERIAL_DEBUG is defined above in the
 * current header
 * 
 *@param buf: array of bytes
 *@param buf_len: length of bytes to be written to the Serial Monitor
 */
void printHex_buf(uint8_t * buf, uint8_t buf_len);




/* EPS related functions */

/*@brief Initialisation of ebv_esp_master and message pack to set an Event
 */
void arduino_esp_master_init(void);

/*@brief Check if ESP Slave is ready ready
 *@return true - ESP Slave ready; false - not ready
 */
bool arduino_esp_master_check_wait_slave_ready(void);

/*@brief Build a message pack with given parameters
 *
 * NOTE! The length of data here shall not be more than ARDUINO_ESP_MASTER_I2C_MAX_DATA_SIZE
 * 
 *@return true - meet all requirements; false - length of final message is too big
 */
bool arduino_esp_master_build_event(char * event_name, uint8_t event_name_size, 
                                    char * parameter_name, uint8_t parameter_name_size,
                                    uint64_t parameter_value);

/*@brief Send the built message pack with the Event data to the Cloud
 *@return true - on success; false - Put Event has failed
 */
bool arduino_esp_master_send_event(void);


#endif /* _ARDUINO_ESP_MASTER_H_ */
