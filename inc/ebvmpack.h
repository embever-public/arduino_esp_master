/*
 * ebvmpack.h
 *
 *  Created on: 6 Feb 2018
 *      Author: pablo
 */

#ifndef INC_EBVMPACK_H_
#define INC_EBVMPACK_H_


#include "extcwpack.h"

typedef enum {
	EBV_MPACK_ACTION_TYPE_CONNECT,
	EBV_MPACK_ACTION_TYPE_DISCONNECT,
	EBV_MPACK_ACTION_TYPE_REQUESTS,
	EBV_MPACK_ACTION_TYPE_UNKNOWN,
} ebv_mpack_action_type;

typedef enum {
	EBV_MPACK_MSG_TYPE_DISCONNECT,
	EBV_MPACK_MSG_TYPE_ACTIONS,
	EBV_MPACK_MSG_TYPE_RESULTS,
	EBV_MPACK_MSG_TYPE_EVENTS,
	EBV_MPACK_MSG_TYPE_UNKNOWN,
} ebv_mpack_msg_type;

struct ebv_mpack_requests_object {
	cwpack_blob type;
	uint64_t id;
	char *status;
};

struct ebv_mpack_requests_payload {
	uint32_t num;  /* number of request objects in list */
	struct ebv_mpack_requests_object *obj;
};

struct ebv_mpack {
	cw_unpack_context *unpack;
	cw_pack_context *pack;
	ebv_mpack_action_type type;
	ebv_mpack_msg_type msg_type;
	struct ebv_mpack_requests_payload *payload;
};

/**
 *@brief Initialize an ebv_mpack structure for given unpack/pack contexts
 */
void ebv_mpack_init_context(struct ebv_mpack *mpack,
		void *rxbuf, uint32_t rxbuflen,
		void *txbuf, uint32_t txbuflen,
		cw_unpack_context *unpack, cw_pack_context *pack);

/**
 *@brief Initialize an ebv_mpack structure (deprecated)
 *
 * This function uses static unpack/pack context. Every call to this function,
 * this context are reset.
 */
void ebv_mpack_init(struct ebv_mpack *mpack,
		void *rxbuf, uint32_t rxbuflen,
		void *txbuf, uint32_t txbuflen);

/* Emvbever IoT Protocol v2.0 */
void ebv_mpack_get_msg_type(struct ebv_mpack *mpack);
void ebv_mpack_get_actions_list(struct ebv_mpack *mpack);
void ebv_mpack_build_results_message_start(struct ebv_mpack *mpack);
void ebv_mpack_build_public_results_start(struct ebv_mpack *mpack);
void ebv_mpack_build_private_results_start(struct ebv_mpack *mpack);
void ebv_mpack_build_private_events_start(struct ebv_mpack *mpack);
void ebv_mpack_get_next_actions_object(struct ebv_mpack *mpack,
		struct ebv_mpack_requests_object *obj);
void ebv_mpack_get_next_private_results_list(struct ebv_mpack *mpack,
		struct ebv_mpack_requests_object *obj);
void ebv_mpack_set_next_results_object(struct ebv_mpack *mpack,
		struct ebv_mpack_requests_object *obj, uint32_t length);
void ebv_mpack_set_next_events_object(struct ebv_mpack *mpack,
		struct ebv_mpack_requests_object *obj);
/* Emvbever IoT Protocol v1.x */
void ebv_mpack_get_action_type(struct ebv_mpack *mpack);
void ebv_mpack_get_requests_objects_list(struct ebv_mpack *mpack);
void ebv_mpack_build_responses_start(struct ebv_mpack *mpack);
void ebv_mpack_build_events_start(struct ebv_mpack *mpack);
void ebv_mpack_set_next_events_object_v10(struct ebv_mpack *mpack, struct ebv_mpack_requests_object *obj);
void ebv_mpack_get_next_requests_object(struct ebv_mpack *mpack, struct ebv_mpack_requests_object *obj);
void ebv_mpack_set_next_responses_object(struct ebv_mpack *mpack, struct ebv_mpack_requests_object *obj);
int ebv_mpack_strcmp(char *str, cwpack_blob *cwpstr);
void ebv_mpack_add_str(struct ebv_mpack *mpack, char *str);
bool ebv_mpack_is_str(char *refstr, struct ebv_mpack *mpack);

#endif /* INC_EBVMPACK_H_ */
